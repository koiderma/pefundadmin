import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForecastComponent } from './components/forecast/forecast.component';
import { PeFundComponent } from './components/pe-fund/pe-fund.component';
import { PefundCreateComponent } from './components/pefund-create/pefund-create.component';
import { PefundEditComponent } from './components/pefund-edit/pefund-edit.component';
import { CommitmentComponent } from './components/commitment/commitment.component';
import { CommitmentCreateComponent } from './components/commitment-create/commitment-create.component';
import { CommitmentEditComponent } from './components/commitment-edit/commitment-edit.component';
import { NavComponent } from './components/nav/nav.component';
import { NavCreateComponent } from './components/nav-create/nav-create.component';
import { NavEditComponent } from './components/nav-edit/nav-edit.component';
import { CashflowComponent } from './components/cashflow/cashflow.component';
import { CashflowCreateComponent } from './components/cashflow-create/cashflow-create.component';
import { CashflowEditComponent } from './components/cashflow-edit/cashflow-edit.component';

const routes: Routes = [
  { path: 'forecast', component: ForecastComponent},
  { path: 'pe-fund', component: PeFundComponent},
  { path: 'commitment', component: CommitmentComponent},
  { path: 'nav', component: NavComponent},
  { path: 'cashflow', component: CashflowComponent},
  { path: 'pe-fund/pe-fund-new', component: PefundCreateComponent},
  { path: 'commitment/commitment-new', component: CommitmentCreateComponent},
  { path: 'nav/nav-new', component: NavCreateComponent},
  { path: 'cashflow/cashflow-new', component: CashflowCreateComponent},
  { path: 'pe-fund/pe-fund-edit/:id', component: PefundEditComponent },
  { path: 'commitment/commitment-edit/:id', component: CommitmentEditComponent },
  { path: 'nav/nav-edit/:id', component: NavEditComponent },
  { path: 'cashflow/cashflow-edit/:id', component: CashflowEditComponent },
  { path: '', redirectTo: 'forecast', pathMatch: 'full' },
  { path: '**', redirectTo: 'forecast', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export class Nav {
  id: number;
  navDate: Date;
  capitalSize: number;
  pensionFundName: string;
  peFundName: string;
}

export class Pefund {
  id: number;
  ticker: string;
  name: string
  startDate: Date;
  investPeriodDate: Date;
  endDate: Date;
  peFundType: string;
}

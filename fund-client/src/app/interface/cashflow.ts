export class Cashflow {
  id: number;
  paymentDate: Date;
  cashFlow: number;
  pensionFundName: string;
  peFundName: string;
  cashFlowType: string;
}

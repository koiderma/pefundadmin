export class Forecast {
  clientFundName: string;
  peFundName: string;
  latestNAV: number;
  cashOutFlows: number[];
  cashInFlows: number[];
  navs: number[];
}

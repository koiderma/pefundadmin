export class Commitment {
  id: number;
  commitmentSize: number;
  pensionFundName: string;
  peFundName: string;
}

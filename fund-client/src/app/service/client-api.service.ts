import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Client } from '../interface/client';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientApiService {

  endpoint = environment.apiUrl+'clients';

  constructor(private httpClient: HttpClient) { }

  getClients(): Observable<Client>{
    return this.httpClient.get<Client>(this.endpoint)
    .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}

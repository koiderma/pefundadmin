import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pefund } from '../interface/pefund';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PefundApiService {

  endpoint = environment.apiUrl+'pefunds';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  getPeFunds(): Observable<Pefund>{
    return this.httpClient.get<Pefund>(this.endpoint)
    .pipe(catchError(this.handleError))
  }

  getPeFund(id): Observable<Pefund> {
    return this.httpClient.get<Pefund>(this.endpoint + '/'  + id)
    .pipe(catchError(this.handleError))
  }

  createPeFund(pefund): Observable<Pefund> {
    console.log("createPeFund",pefund);
    return this.httpClient.post<Pefund>(this.endpoint, JSON.stringify(pefund), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  updatePeFund(id, fund): Observable<Pefund> {
    console.log("updatePeFund",fund);
    return this.httpClient.put<Pefund>(this.endpoint + '/'  + id, JSON.stringify(fund), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  deleteFund(id){
    console.log("deleteFund",id);
    return this.httpClient.delete<Pefund>(this.endpoint+ '/' + id, this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}

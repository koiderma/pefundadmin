import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Forecast } from '../interface/forecast';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForecastApiService {

  endpoint = environment.apiUrl+'forecast';

  constructor(private httpClient: HttpClient) { }

  getForecast(id: number): Observable<Forecast>{
     return this.httpClient.get<Forecast>(this.endpoint + '/'  + id)
     .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}

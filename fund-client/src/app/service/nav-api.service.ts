import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Nav } from '../interface/nav';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NavApiService {

  endpoint = environment.apiUrl+'nav';

    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

  constructor(private httpClient: HttpClient) { }

  getAllNavs(id: number): Observable<Nav>{
    return this.httpClient.get<Nav>(this.endpoint + '/all/'  + id)
    .pipe(catchError(this.handleError))
  }

  getNav(id: number): Observable<Nav>{
    return this.httpClient.get<Nav>(this.endpoint + '/'  + id)
    .pipe(catchError(this.handleError))
  }

  createNav(nav): Observable<Nav> {
    console.log("createNav",nav);
    return this.httpClient.post<Nav>(this.endpoint, JSON.stringify(nav), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  deleteNav(id){
    console.log("deleteNav", id);
    return this.httpClient.delete<Nav>(this.endpoint+ '/' + id, this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  updateNav(id, nav): Observable<Nav> {
    console.log("updateNav",nav);
    return this.httpClient.put<Nav>(this.endpoint + '/'  + id, JSON.stringify(nav), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Peftype } from '../interface/peftype';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PeftypeApiService {

  endpoint = environment.apiUrl+'peft';

  constructor(private httpClient: HttpClient) { }

  getPeftypes(): Observable<Peftype>{
    return this.httpClient.get<Peftype>(this.endpoint)
    .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}

import { TestBed } from '@angular/core/testing';

import { CommitmentApiService } from './commitment-api.service';

describe('CommitmentApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommitmentApiService = TestBed.get(CommitmentApiService);
    expect(service).toBeTruthy();
  });
});

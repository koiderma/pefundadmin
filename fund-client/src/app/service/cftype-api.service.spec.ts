import { TestBed } from '@angular/core/testing';

import { CftypeApiService } from './cftype-api.service';

describe('CftypeApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CftypeApiService = TestBed.get(CftypeApiService);
    expect(service).toBeTruthy();
  });
});

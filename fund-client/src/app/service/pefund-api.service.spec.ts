import { TestBed } from '@angular/core/testing';

import { PefundApiService } from './pefund-api.service';

describe('PefundApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PefundApiService = TestBed.get(PefundApiService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Commitment } from '../interface/commitment';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommitmentApiService {

  endpoint = environment.apiUrl+'commitment';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  getAllCommitments(id: number): Observable<Commitment>{
    return this.httpClient.get<Commitment>(this.endpoint + '/all/'  + id)
    .pipe(catchError(this.handleError))
  }

  getCommitment(id: number): Observable<Commitment>{
    return this.httpClient.get<Commitment>(this.endpoint + '/'  + id)
    .pipe(catchError(this.handleError))
  }

  createCommitment(com): Observable<Commitment> {
    console.log("createCommitment",com);
    return this.httpClient.post<Commitment>(this.endpoint, JSON.stringify(com), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  deleteCommitment(id){
    console.log("deleteCommitment",id);
    return this.httpClient.delete<Commitment>(this.endpoint+ '/' + id, this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  updateCommitment(id, com): Observable<Commitment> {
    console.log("updateCommitment",com);
    return this.httpClient.put<Commitment>(this.endpoint + '/'  + id, JSON.stringify(com), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

}

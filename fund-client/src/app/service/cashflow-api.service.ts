import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cashflow } from '../interface/cashflow';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CashflowApiService {

  endpoint = environment.apiUrl+'cf';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private httpClient: HttpClient) { }

  getAllCashflows(id: number): Observable<Cashflow>{
    return this.httpClient.get<Cashflow>(this.endpoint + '/all/'  + id)
    .pipe(catchError(this.handleError))
  }

  getCashflow(id: number): Observable<Cashflow>{
    return this.httpClient.get<Cashflow>(this.endpoint + '/'  + id)
    .pipe(catchError(this.handleError))
  }

  createCashflow(cf): Observable<Cashflow> {
    console.log("createCashflow",cf);
    return this.httpClient.post<Cashflow>(this.endpoint, JSON.stringify(cf), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  deleteCashflow(id){
    console.log("deleteCashflow",id);
    return this.httpClient.delete<Cashflow>(this.endpoint+ '/' + id, this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  updateCashflow(id, cf): Observable<Cashflow> {
    console.log("updateCashflow",cf);
    return this.httpClient.put<Cashflow>(this.endpoint + '/'  + id, JSON.stringify(cf), this.httpOptions)
    .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}

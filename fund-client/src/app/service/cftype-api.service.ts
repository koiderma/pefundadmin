import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cftype } from '../interface/cftype';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CftypeApiService {

  endpoint = environment.apiUrl+'cft';

  constructor(private httpClient: HttpClient) { }

  getCftypes(): Observable<Cftype>{
    return this.httpClient.get<Cftype>(this.endpoint)
    .pipe(catchError(this.handleError))
  }

  handleError(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}

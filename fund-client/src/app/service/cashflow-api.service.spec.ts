import { TestBed } from '@angular/core/testing';

import { CashflowApiService } from './cashflow-api.service';

describe('CashflowApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CashflowApiService = TestBed.get(CashflowApiService);
    expect(service).toBeTruthy();
  });
});

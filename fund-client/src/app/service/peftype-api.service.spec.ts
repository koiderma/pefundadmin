import { TestBed } from '@angular/core/testing';

import { PeftypeApiService } from './peftype-api.service';

describe('PeftypeApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PeftypeApiService = TestBed.get(PeftypeApiService);
    expect(service).toBeTruthy();
  });
});

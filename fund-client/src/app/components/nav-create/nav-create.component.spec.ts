import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavCreateComponent } from './nav-create.component';

describe('NavCreateComponent', () => {
  let component: NavCreateComponent;
  let fixture: ComponentFixture<NavCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

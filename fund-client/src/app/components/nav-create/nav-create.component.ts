import { Component, OnInit, Input } from '@angular/core';
import { ClientApiService } from '../../service/client-api.service';
import { PefundApiService } from '../../service/pefund-api.service';
import { Router } from '@angular/router';
import { Client } from '../../interface/client';
import { Pefund } from '../../interface/pefund';
import { NavApiService } from '../../service/nav-api.service';

@Component({
  selector: 'app-nav-create',
  templateUrl: './nav-create.component.html',
  styleUrls: ['./nav-create.component.css']
})
export class NavCreateComponent implements OnInit {

  clients: any = [];
  pefunds: any = [];
  @Input() nav = { id: 0, pensionFundName: '', peFundName: '', capitalSize: 0, navDate: 0}

  constructor(
    public clientApi: ClientApiService,
    public navApi: NavApiService,
    public pefundApi: PefundApiService,
    public router: Router
  ) { }

  ngOnInit() {
    this.loadClients();
    this.loadFunds();
  }

  loadClients(){
    this.clientApi.getClients().subscribe((res: {}) =>{
      this.clients = res;
    });
  }

  loadFunds(){
    this.pefundApi.getPeFunds().subscribe((res: {}) =>{
      this.pefunds = res;
    });
  }

  addNav(nav) {
    console.log("addNav",nav);
    this.navApi.createNav(this.nav).subscribe((data: {}) => {
      this.router.navigate(['/nav'])
    })
  }

}

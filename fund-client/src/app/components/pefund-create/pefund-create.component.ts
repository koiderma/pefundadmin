import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PefundApiService } from '../../service/pefund-api.service';
import { Peftype } from '../../interface/peftype';
import { PeftypeApiService } from '../../service/peftype-api.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-pefund-create',
  templateUrl: './pefund-create.component.html',
  styleUrls: ['./pefund-create.component.css']
})
export class PefundCreateComponent implements OnInit {

  @Input() fundDetails = { id: 0, ticker: '', name: '', peFundType: '', startDate: 0, investPeriodDate: 0, endDate: 0 }
  peftypes: any = [];

  constructor(
    public pefundApi: PefundApiService,
    public peftypeApi: PeftypeApiService,
    public router: Router
  ) { }

  ngOnInit() {
    this.loadPeftypes();
  }

  loadPeftypes(){
    this.peftypeApi.getPeftypes().subscribe((res: {}) =>{
      this.peftypes = res;
    });
  }

  addFund(dataFund) {
    console.log("addFund", dataFund);
    this.pefundApi.createPeFund(this.fundDetails).subscribe((data: {}) => {
      this.router.navigate(['/pe-fund'])
    })
  }

}

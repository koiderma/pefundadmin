import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PefundCreateComponent } from './pefund-create.component';

describe('PefundCreateComponent', () => {
  let component: PefundCreateComponent;
  let fixture: ComponentFixture<PefundCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PefundCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PefundCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

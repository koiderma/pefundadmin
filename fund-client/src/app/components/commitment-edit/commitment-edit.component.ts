import { Component, OnInit } from '@angular/core';
import { CommitmentApiService } from '../../service/commitment-api.service';
import { Commitment } from '../../interface/commitment';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-commitment-edit',
  templateUrl: './commitment-edit.component.html',
  styleUrls: ['./commitment-edit.component.css']
})
export class CommitmentEditComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  commitment: any = {};

  constructor(
      public commitmentApi: CommitmentApiService,
      public actRoute: ActivatedRoute,
      public router: Router) { }

  ngOnInit() {
    this.commitmentApi.getCommitment(this.id).subscribe((data: {}) => {
       this.commitment= data;
       console.log("ngOnInit",this.commitment);
    })
  }

  updateCommitment() {
    if(window.confirm('Are you sure, you want to update?')){
      this.commitmentApi.updateCommitment(this.id, this.commitment).subscribe(data => {
        this.router.navigate(['/commitment'])
      })
    }
  }

}

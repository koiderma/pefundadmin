import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitmentEditComponent } from './commitment-edit.component';

describe('CommitmentEditComponent', () => {
  let component: CommitmentEditComponent;
  let fixture: ComponentFixture<CommitmentEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommitmentEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitmentEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommitmentCreateComponent } from './commitment-create.component';

describe('CommitmentCreateComponent', () => {
  let component: CommitmentCreateComponent;
  let fixture: ComponentFixture<CommitmentCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommitmentCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommitmentCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

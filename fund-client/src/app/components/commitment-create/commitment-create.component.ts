import { Component, OnInit, Input } from '@angular/core';
import { ClientApiService } from '../../service/client-api.service';
import { PefundApiService } from '../../service/pefund-api.service';
import { Router } from '@angular/router';
import { Client } from '../../interface/client';
import { Pefund } from '../../interface/pefund';
import { CommitmentApiService } from '../../service/commitment-api.service';

@Component({
  selector: 'app-commitment-create',
  templateUrl: './commitment-create.component.html',
  styleUrls: ['./commitment-create.component.css']
})
export class CommitmentCreateComponent implements OnInit {

  clients: any = [];
  pefunds: any = [];
  @Input() commitment = { id: 0, pensionFundName: '', peFundName: '', commitmentSize: 0}

  constructor(
    public clientApi: ClientApiService,
    public commitmentApi: CommitmentApiService,
    public pefundApi: PefundApiService,
    public router: Router
   ) { }

  ngOnInit() {
    this.loadClients();
    this.loadFunds();
  }

  loadClients(){
    this.clientApi.getClients().subscribe((res: {}) =>{
      this.clients = res;
    });
  }

   loadFunds(){
     this.pefundApi.getPeFunds().subscribe((res: {}) =>{
       this.pefunds = res;
     });
   }

   addCommitment(com) {
     console.log("addCommitment",com);
     this.commitmentApi.createCommitment(this.commitment).subscribe((data: {}) => {
       this.router.navigate(['/commitment'])
     })
   }

}

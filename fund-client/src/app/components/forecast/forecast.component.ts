import { Component, OnInit } from '@angular/core';
import { ClientApiService } from '../../service/client-api.service';
import { ForecastApiService } from '../../service/forecast-api.service';
import { Client } from '../../interface/client';
import { Forecast } from '../../interface/forecast';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {

  title = 'fund-client';
    clients: any = [];
    forecast;

    constructor(public clientApi: ClientApiService, public forecastApi: ForecastApiService) {}
    ngOnInit() {
      this.clientApi.getClients().subscribe((res: {}) =>{
        console.log("ngOnInit:", res);
        this.clients = res;
      });

    }

    changeClients(value:any):void {
      this.forecastApi.getForecast(value).subscribe(res=>{
        console.log("changeClients",res);
        this.forecast = res;
      });
    }

}

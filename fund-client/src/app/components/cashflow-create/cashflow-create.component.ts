import { Component, OnInit, Input } from '@angular/core';
import { ClientApiService } from '../../service/client-api.service';
import { PefundApiService } from '../../service/pefund-api.service';
import { Router } from '@angular/router';
import { Client } from '../../interface/client';
import { Pefund } from '../../interface/pefund';
import { Cftype } from '../../interface/cftype';
import { CashflowApiService } from '../../service/cashflow-api.service';
import { CftypeApiService } from '../../service/cftype-api.service';

@Component({
  selector: 'app-cashflow-create',
  templateUrl: './cashflow-create.component.html',
  styleUrls: ['./cashflow-create.component.css']
})
export class CashflowCreateComponent implements OnInit {

  clients: any = [];
  pefunds: any = [];
  cftypes: any = [];
  @Input() cashflow = { id: 0, pensionFundName: '', peFundName: '', cashFlowType: '', cashFlow: 0, paymentDate: 0}

  constructor(
    public clientApi: ClientApiService,
    public cashflowApi: CashflowApiService,
    public pefundApi: PefundApiService,
    public cftypeApi: CftypeApiService,
    public router: Router
  ) { }

  ngOnInit() {
    this.loadClients();
    this.loadFunds();
    this.loadCftypes();
  }

  loadClients(){
    this.clientApi.getClients().subscribe((res: {}) =>{
      this.clients = res;
    });
  }

  loadFunds(){
    this.pefundApi.getPeFunds().subscribe((res: {}) =>{
      this.pefunds = res;
    });
  }

  loadCftypes(){
    this.cftypeApi.getCftypes().subscribe((res: {}) =>{
      this.cftypes = res;
    });
  }

  addCashflow(cf) {
    console.log("addCashFlow",cf);
    this.cashflowApi.createCashflow(this.cashflow).subscribe((data: {}) => {
      this.router.navigate(['/cashflow'])
    })
  }


}

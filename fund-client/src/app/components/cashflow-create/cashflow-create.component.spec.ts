import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashflowCreateComponent } from './cashflow-create.component';

describe('CashflowCreateComponent', () => {
  let component: CashflowCreateComponent;
  let fixture: ComponentFixture<CashflowCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashflowCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashflowCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

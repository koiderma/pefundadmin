import { Component, OnInit } from '@angular/core';
import { PefundApiService } from '../../service/pefund-api.service';
import { Pefund } from '../../interface/pefund';

@Component({
  selector: 'app-pe-fund',
  templateUrl: './pe-fund.component.html',
  styleUrls: ['./pe-fund.component.css']
})
export class PeFundComponent implements OnInit {

  pefunds: any = [];

  constructor(public pefundApi: PefundApiService) {}
  ngOnInit() {
    this.loadFunds()
  }

  loadFunds(){
    this.pefundApi.getPeFunds().subscribe((res: {}) =>{
      console.log("loadFunds",res);
      this.pefunds = res;
    });
  }

  deleteFund(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.pefundApi.deleteFund(id).subscribe(data => {
        this.loadFunds()
      })
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeFundComponent } from './pe-fund.component';

describe('PeFundComponent', () => {
  let component: PeFundComponent;
  let fixture: ComponentFixture<PeFundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeFundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeFundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

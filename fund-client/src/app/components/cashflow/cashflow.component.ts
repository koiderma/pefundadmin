import { Component, OnInit } from '@angular/core';
import { CashflowApiService } from '../../service/cashflow-api.service';
import { PefundApiService } from '../../service/pefund-api.service';
import { Cashflow } from '../../interface/cashflow';
import { Pefund } from '../../interface/pefund';
import { Router} from '@angular/router';

@Component({
  selector: 'app-cashflow',
  templateUrl: './cashflow.component.html',
  styleUrls: ['./cashflow.component.css']
})
export class CashflowComponent implements OnInit {

  funds: any = [];
  cashflow;
  activeFund;

  constructor(public pefundApi: PefundApiService, public cashflowApi: CashflowApiService,
    public router: Router) { }

  ngOnInit() {
    this.loadFunds();
  }

  loadFunds(){
    this.pefundApi.getPeFunds().subscribe((res: {}) =>{
      console.log("loadFunds", res);
      this.funds = res;
    });
  }

  changeFunds(value:any):void {
    this.activeFund = value;
    this.cashflowApi.getAllCashflows(value).subscribe(res=>{
      console.log("changeFunds:",res);
      this.cashflow = res;
    });
  }

  deleteCashflow(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.cashflowApi.deleteCashflow(id).subscribe(data => {
        this.changeFunds(this.activeFund);
      })
    }
  }


}

import { Component, OnInit } from '@angular/core';
import { NavApiService } from '../../service/nav-api.service';
import { Nav } from '../../interface/nav';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-nav-edit',
  templateUrl: './nav-edit.component.html',
  styleUrls: ['./nav-edit.component.css']
})
export class NavEditComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  nav: any = {};

  constructor(
    public navApi: NavApiService,
    public actRoute: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    this.navApi.getNav(this.id).subscribe((data: {}) => {
      this.nav = data;
      console.log("ngOnInit",this.nav);
    })
  }

  updateNav() {
    if(window.confirm('Are you sure, you want to update?')){
      this.navApi.updateNav(this.id, this.nav).subscribe(data => {
        this.router.navigate(['/nav'])
      })
    }
  }

}

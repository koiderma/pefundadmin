import { Component, OnInit } from '@angular/core';
import { CashflowApiService } from '../../service/cashflow-api.service';
import { Cashflow } from '../../interface/cashflow';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-cashflow-edit',
  templateUrl: './cashflow-edit.component.html',
  styleUrls: ['./cashflow-edit.component.css']
})
export class CashflowEditComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  cashflow: any = {};

  constructor(
    public cashflowApi: CashflowApiService,
    public actRoute: ActivatedRoute,
    public router: Router) { }

    ngOnInit() {
      this.cashflowApi.getCashflow(this.id).subscribe((data: {}) => {
        this.cashflow = data;
        console.log("ngOnInit",this.cashflow);
      })
    }

    updateCashflow() {
      if(window.confirm('Are you sure, you want to update?')){
        this.cashflowApi.updateCashflow(this.id, this.cashflow).subscribe(data => {
          this.router.navigate(['/cashflow'])
        })
      }
    }

}

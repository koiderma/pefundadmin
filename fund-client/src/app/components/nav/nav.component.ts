import { Component, OnInit } from '@angular/core';
import { NavApiService } from '../../service/nav-api.service';
import { PefundApiService } from '../../service/pefund-api.service';
import { Nav } from '../../interface/nav';
import { Pefund } from '../../interface/pefund';
import { Router} from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  funds: any = [];
  nav;
  activeFund;

  constructor(public pefundApi: PefundApiService, public navApi: NavApiService,
                 public router: Router) { }

  ngOnInit() {
    this.loadFunds();
  }

  loadFunds(){
    this.pefundApi.getPeFunds().subscribe((res: {}) =>{
      this.funds = res;
    });
  }

  changeFunds(value:any):void {
    this.activeFund = value;
    this.navApi.getAllNavs(value).subscribe(res=>{
      console.log("changeFunds",res);
      this.nav = res;
    });
  }

  deleteNav(id) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.navApi.deleteNav(id).subscribe(data => {
        this.changeFunds(this.activeFund);
      })
    }
  }

}

import { Component, OnInit } from '@angular/core';
import { PefundApiService } from '../../service/pefund-api.service';
import { Pefund } from '../../interface/pefund';
import { ActivatedRoute, Router } from '@angular/router';
import { Peftype } from '../../interface/peftype';
import { PeftypeApiService } from '../../service/peftype-api.service';

@Component({
  selector: 'app-pefund-edit',
  templateUrl: './pefund-edit.component.html',
  styleUrls: ['./pefund-edit.component.css']
})
export class PefundEditComponent implements OnInit {

  id = this.actRoute.snapshot.params['id'];
  fundDetails: any = {};
  peftypes: any = [];

  constructor(
    public pefundApi: PefundApiService,
    public peftypeApi: PeftypeApiService,
    public actRoute: ActivatedRoute,
    public router: Router) { }

  ngOnInit() {
    this.pefundApi.getPeFund(this.id).subscribe((data: {}) => {
      this.fundDetails= data;
      console.log("ngOnInit",this.fundDetails);
    })
    this.loadPeftypes();
  }

  loadPeftypes(){
    this.peftypeApi.getPeftypes().subscribe((res: {}) =>{
      this.peftypes = res;
    });
  }

  updateFund() {
      if(window.confirm('Are you sure, you want to update?')){
        this.pefundApi.updatePeFund(this.id, this.fundDetails).subscribe(data => {
          this.router.navigate(['/pe-fund'])
        })
      }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PefundEditComponent } from './pefund-edit.component';

describe('PefundEditComponent', () => {
  let component: PefundEditComponent;
  let fixture: ComponentFixture<PefundEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PefundEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PefundEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

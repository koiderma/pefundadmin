import { Component, OnInit } from '@angular/core';
import { ClientApiService } from '../../service/client-api.service';
import { CommitmentApiService } from '../../service/commitment-api.service';
import { Client } from '../../interface/client';
import { Commitment } from '../../interface/commitment';
import { Router} from '@angular/router';

@Component({
  selector: 'app-commitment',
  templateUrl: './commitment.component.html',
  styleUrls: ['./commitment.component.css']
})
export class CommitmentComponent implements OnInit {

  clients: any = [];
  commitment;
  activeFund:number;

  constructor(public clientApi: ClientApiService, public commitmentApi: CommitmentApiService,
    public router: Router) { }

  ngOnInit() {
    this.loadClients()
  }

  loadClients(){
    this.clientApi.getClients().subscribe((res: {}) =>{
      console.log("loadClients", res);
      this.clients = res;
    });
  }

    changeClients(value:any):void {
      this.activeFund = value;
      this.commitmentApi.getAllCommitments(value).subscribe(res=>{
        console.log("changeClients",res);
        this.commitment = res;
      });
    }

    deleteCommitment(id) {
      if (window.confirm('Are you sure, you want to delete?')){
        this.commitmentApi.deleteCommitment(id).subscribe(data => {
          this.changeClients(this.activeFund);
        })
      }
    }

}

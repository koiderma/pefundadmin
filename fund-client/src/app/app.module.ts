import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ForecastComponent } from './components/forecast/forecast.component';
import { PeFundComponent } from './components/pe-fund/pe-fund.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PefundCreateComponent } from './components/pefund-create/pefund-create.component';
import { PefundEditComponent } from './components/pefund-edit/pefund-edit.component';
import { CommitmentComponent } from './components/commitment/commitment.component';
import { CommitmentCreateComponent } from './components/commitment-create/commitment-create.component';
import { CommitmentEditComponent } from './components/commitment-edit/commitment-edit.component';
import { NavComponent } from './components/nav/nav.component';
import { NavCreateComponent } from './components/nav-create/nav-create.component';
import { NavEditComponent } from './components/nav-edit/nav-edit.component';
import { CashflowComponent } from './components/cashflow/cashflow.component';
import { CashflowCreateComponent } from './components/cashflow-create/cashflow-create.component';
import { CashflowEditComponent } from './components/cashflow-edit/cashflow-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    ForecastComponent,
    PeFundComponent,
    PefundCreateComponent,
    PefundEditComponent,
    CommitmentComponent,
    CommitmentCreateComponent,
    CommitmentEditComponent,
    NavComponent,
    NavCreateComponent,
    NavEditComponent,
    CashflowComponent,
    CashflowCreateComponent,
    CashflowEditComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

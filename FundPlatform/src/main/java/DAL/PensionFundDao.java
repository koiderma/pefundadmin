package DAL;

import Domain.PensionFund;

import javax.persistence.EntityManager;
import java.util.List;

public class PensionFundDao implements Dao<PensionFund> {

    private static EntityManager entityManager = SessionUtil.getEntityManager();

    @Override
    public PensionFund get(int id) {
        entityManager.getTransaction().begin();
        PensionFund fund1 = entityManager.find(PensionFund.class, id);
        entityManager.getTransaction().commit();
        return fund1;
    }

    @Override
    public List<PensionFund> getAll() {
        entityManager.getTransaction().begin();
        List<PensionFund> result = entityManager.createQuery("SELECT a FROM PensionFund a", PensionFund.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void save(PensionFund pensionFund) {
        entityManager.getTransaction().begin();
        entityManager.persist(pensionFund);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(PensionFund pensionFund, String[] params) {

    }

    @Override
    public void delete(PensionFund pensionFund) {
        entityManager.getTransaction().begin();
        entityManager.remove(pensionFund);
        entityManager.getTransaction().commit();
    }

    public PensionFund getFundByTicker(String name) {
        entityManager.getTransaction().begin();
        PensionFund client = entityManager.
                createQuery("SELECT a FROM PensionFund a WHERE a.name = ?1", PensionFund.class)
                .setParameter(1, name)
                .getSingleResult();

        entityManager.getTransaction().commit();
        return client;
    }

}

package DAL;

import Domain.NavStatement;

import javax.persistence.EntityManager;
import java.util.List;

public class NavStatementDao implements Dao<NavStatement> {

    private static EntityManager entityManager = SessionUtil.getEntityManager();

    @Override
    public NavStatement get(int id) {
        entityManager.getTransaction().begin();
        NavStatement nav = entityManager.find(NavStatement.class, id);
        entityManager.getTransaction().commit();
        return nav;
    }

    @Override
    public List<NavStatement> getAll() {
        entityManager.getTransaction().begin();
        List<NavStatement> result = entityManager.createQuery("SELECT a FROM NavStatement a", NavStatement.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void save(NavStatement nav) {
        entityManager.getTransaction().begin();

        entityManager.createNativeQuery("INSERT INTO navstatement (pefund_id, pensionfund_id, navdate, capitalsize) VALUES (?,?,?,?)")
                .setParameter(1, nav.getPeFund().getId())
                .setParameter(2, nav.getPensionFund().getId())
                .setParameter(3, nav.getNavDate())
                .setParameter(4, nav.getCapitalSize())
                .executeUpdate();

        entityManager.getTransaction().commit();
    }

    @Override
    public void update(NavStatement nav, String[] params) {

    }

    @Override
    public void delete(NavStatement nav) {
        entityManager.getTransaction().begin();
        entityManager.remove(nav);
        entityManager.getTransaction().commit();
    }

    public NavStatement getLatestNavByFunds(int pef_id, int pe_id) {
        entityManager.getTransaction().begin();
        NavStatement nav = new NavStatement();

        try {
            nav = entityManager.
                    createQuery("SELECT a FROM NavStatement a WHERE (a.peFundNAV.id = ?1 and a.pensionFundNAV.id =?2) " +
                            "and a.navDate = (SELECT MAX(e.navDate) FROM NavStatement e " +
                            "WHERE (e.peFundNAV.id = ?1 and e.pensionFundNAV.id =?2))", NavStatement.class)
                    .setParameter(1, pe_id)
                    .setParameter(2, pef_id)
                    .getSingleResult();
        } catch (Exception e) {
            nav.setCapitalSize(0);
        }

        entityManager.getTransaction().commit();
        return nav;
    }

    public NavStatement getLatestNavByFundsBeforeDate(int pef_id, int pe_id, java.sql.Date cutOffDate) {
        entityManager.getTransaction().begin();
        NavStatement nav = new NavStatement();

        try {
            nav = entityManager.
                    createQuery("SELECT a FROM NavStatement a WHERE (a.peFundNAV.id = ?1 and a.pensionFundNAV.id =?2) " +
                            "and a.navDate = (SELECT MAX(e.navDate) FROM NavStatement e " +
                            "WHERE (e.peFundNAV.id = ?1 and e.pensionFundNAV.id =?2 and e.navDate <= ?3))", NavStatement.class)
                    .setParameter(1, pe_id)
                    .setParameter(2, pef_id)
                    .setParameter(3, cutOffDate)
                    .getSingleResult();
        } catch (Exception e) {
            nav.setCapitalSize(0);
        }

        entityManager.getTransaction().commit();
        return nav;
    }

    public int getNavByFundsAndDate(int pef_id, int pe_id, java.sql.Date navDate) {

        entityManager.getTransaction().begin();
        int capitalSize;
        try {
            capitalSize = (int) entityManager.
                    createQuery("SELECT a.capitalSize FROM NavStatement a WHERE (a.peFundNAV.id = ?1 and a.pensionFundNAV.id =?2) " +
                            "and a.navDate = ?3")
                    .setParameter(1, pe_id)
                    .setParameter(2, pef_id)
                    .setParameter(3, navDate)
                    .getSingleResult();
        } catch (Exception e) {
            capitalSize = -1;
        }
        entityManager.getTransaction().commit();

        return capitalSize;
    }

    public List<NavStatement> getNavByPeFund(int pe_id) {

        entityManager.getTransaction().begin();
        List<NavStatement> result = entityManager.
                createQuery("SELECT a FROM NavStatement a WHERE a.peFundNAV.id = ?1 order by a.navDate desc", NavStatement.class)
                .setParameter(1, pe_id)
                .getResultList();

        entityManager.getTransaction().commit();
        return result;
    }

    public int getClientHistoricalNAV(int client_id, java.sql.Date navDate) {

        entityManager.getTransaction().begin();
        Long totalCapitalSize = (Long) entityManager
                .createQuery("SELECT SUM (a.capitalSize) FROM NavStatement a WHERE(a.pensionFundNAV.id =?1 AND a.navDate =?2)")
                .setParameter(1, client_id)
                .setParameter(2, navDate)
                .getSingleResult();
        entityManager.getTransaction().commit();

        if (totalCapitalSize == null) {
            return -1;
        } else {
            return totalCapitalSize.intValue();
        }
    }

}


package DAL;

import Domain.CfType;

import javax.persistence.EntityManager;
import java.util.List;

public class CfTypeDao implements Dao<CfType> {

    private static EntityManager entityManager = SessionUtil.getEntityManager();

    @Override
    public CfType get(int id) {
        entityManager.getTransaction().begin();
        CfType cft = entityManager.find(CfType.class, id);
        entityManager.getTransaction().commit();
        return cft;
    }

    @Override
    public List<CfType> getAll() {
        entityManager.getTransaction().begin();
        List<CfType> result = entityManager.createQuery("SELECT a FROM CfType a", CfType.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void save(CfType cft) {
        entityManager.getTransaction().begin();
        entityManager.persist(cft);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(CfType cft, String[] params) {

    }

    @Override
    public void delete(CfType cft) {
        entityManager.getTransaction().begin();
        entityManager.remove(cft);
        entityManager.getTransaction().commit();
    }

    public CfType getCftByName(String name) {
        entityManager.getTransaction().begin();
        CfType cft = new CfType();
        cft = entityManager.
                createQuery("SELECT a FROM CfType a WHERE a.name = ?1", CfType.class)
                .setParameter(1, name)
                .getSingleResult();

        entityManager.getTransaction().commit();
        return cft;
    }


}

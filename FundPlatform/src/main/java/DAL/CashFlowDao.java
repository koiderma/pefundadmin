package DAL;

import Domain.CashFlow;

import javax.persistence.EntityManager;
import java.util.List;

public class CashFlowDao implements Dao<CashFlow> {

    private static EntityManager entityManager = SessionUtil.getEntityManager();

    @Override
    public CashFlow get(int id) {
        entityManager.getTransaction().begin();
        CashFlow cf = entityManager.find(CashFlow.class, id);
        entityManager.getTransaction().commit();
        return cf;
    }

    @Override
    public List<CashFlow> getAll() {
        entityManager.getTransaction().begin();
        List<CashFlow> result = entityManager.createQuery("SELECT a FROM CashFlow a", CashFlow.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void save(CashFlow cf) {
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("INSERT INTO CashFlow (pefund_id, pensionfund_id, paymentdate, cashflow, cftype_id) VALUES (?,?,?,?,?)")
                .setParameter(1, cf.getPeFund().getId())
                .setParameter(2, cf.getPensionFund().getId())
                .setParameter(3, cf.getPaymentDate())
                .setParameter(4, cf.getCashFlow())
                .setParameter(5, cf.getCfType().getId())
                .executeUpdate();

        entityManager.getTransaction().commit();
    }

    @Override
    public void update(CashFlow cf, String[] params) {

    }

    @Override
    public void delete(CashFlow cf) {
        entityManager.getTransaction().begin();
        entityManager.remove(cf);
        entityManager.getTransaction().commit();
    }


    public List<CashFlow> getCashFlowByFundsBeforeDate(int pef_id, int pe_id, java.sql.Date cutOffDate) {
        entityManager.getTransaction().begin();
        List<CashFlow> result = entityManager.
                createQuery("SELECT a FROM CashFlow a WHERE (a.peFundCF.id = ?1 and a.pensionFundCF.id =?2 and a.paymentDate <= ?3)", CashFlow.class)
                .setParameter(1, pe_id)
                .setParameter(2, pef_id)
                .setParameter(3, cutOffDate)
                .getResultList();

        entityManager.getTransaction().commit();
        return result;
    }

    public List<CashFlow> getCashFlowByPeFunds(int pe_id) {
        entityManager.getTransaction().begin();
        List<CashFlow> result = entityManager.
                createQuery("SELECT a FROM CashFlow a WHERE a.peFundCF.id = ?1 order by a.paymentDate desc", CashFlow.class)
                .setParameter(1, pe_id)
                .getResultList();

        entityManager.getTransaction().commit();
        return result;
    }

    public int getCashFlowsBetweenDates(int client_id, java.sql.Date startDate, java.sql.Date endDate) {

        entityManager.getTransaction().begin();
        Long totalCashFlow = (Long) entityManager
                .createQuery("SELECT SUM (a.cashFlow) FROM CashFlow a WHERE(a.pensionFundCF.id =?1 AND (a.paymentDate BETWEEN ?2 AND ?3))")
                .setParameter(1, client_id)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getSingleResult();
        entityManager.getTransaction().commit();

        if (totalCashFlow == null) {
            return -1;
        } else {
            return totalCashFlow.intValue();
        }
    }

    public int getCashOutFlowsBetweenDates(int client_id, int pe_id, java.sql.Date startDate, java.sql.Date endDate) {

        entityManager.getTransaction().begin();
        Long totalCashFlow = (Long) entityManager
                .createQuery("SELECT SUM (a.cashFlow) FROM CashFlow a WHERE(a.pensionFundCF.id =?1 AND (a.paymentDate BETWEEN ?2 AND ?3) AND a.peFundCF.id =?4 AND (a.cfType.id = 0 OR a.cfType.id = 3))")
                .setParameter(1, client_id)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .setParameter(4, pe_id)
                .getSingleResult();
        entityManager.getTransaction().commit();

        if (totalCashFlow == null) {
            return 0;
        } else {
            return totalCashFlow.intValue();
        }
    }

    public int getCashOutFlowsBetweenDatesByClient(int client_id,java.sql.Date fundDate, java.sql.Date startDate, java.sql.Date endDate) {

        entityManager.getTransaction().begin();
        Long totalCashFlow = (Long) entityManager
                .createQuery("SELECT SUM (a.cashFlow) FROM CashFlow a WHERE(a.pensionFundCF.id =?1 AND a.peFundCF.startDate <=?2 AND  (a.paymentDate BETWEEN ?3 AND ?4) AND (a.cfType.id = 0 OR a.cfType.id = 3))")
                .setParameter(1, client_id)
                .setParameter(2, fundDate)
                .setParameter(3, startDate)
                .setParameter(4, endDate)
                .getSingleResult();
        entityManager.getTransaction().commit();

        if (totalCashFlow == null) {
            return 0;
        } else {
            return totalCashFlow.intValue();
        }
    }

    public int getCashInFlowsBetweenDates(int client_id, int pe_id, java.sql.Date startDate, java.sql.Date endDate) {

        entityManager.getTransaction().begin();
        Long totalCashFlow = (Long) entityManager
                .createQuery("SELECT SUM (a.cashFlow) FROM CashFlow a WHERE(a.pensionFundCF.id =?1 AND (a.paymentDate BETWEEN ?2 AND ?3) AND a.peFundCF.id =?4 AND (a.cfType.id = 1 OR a.cfType.id = 2))")
                .setParameter(1, client_id)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .setParameter(4, pe_id)
                .getSingleResult();
        entityManager.getTransaction().commit();

        if (totalCashFlow == null) {
            return 0;
        } else {
            return totalCashFlow.intValue();
        }
    }

    public int getCashInFlowsBetweenDatesByClient(int client_id,java.sql.Date fundDate, java.sql.Date startDate, java.sql.Date endDate) {

        entityManager.getTransaction().begin();
        Long totalCashFlow = (Long) entityManager
                .createQuery("SELECT SUM (a.cashFlow) FROM CashFlow a WHERE(a.pensionFundCF.id =?1 AND a.peFundCF.startDate <=?2 AND  (a.paymentDate BETWEEN ?3 AND ?4) AND (a.cfType.id = 1 OR a.cfType.id = 2))")
                .setParameter(1, client_id)
                .setParameter(2, fundDate)
                .setParameter(3, startDate)
                .setParameter(4, endDate)
                .getSingleResult();
        entityManager.getTransaction().commit();

        if (totalCashFlow == null) {
            return 0;
        } else {
            return totalCashFlow.intValue();
        }
    }

}
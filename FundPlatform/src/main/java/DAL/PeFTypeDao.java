package DAL;

import Domain.PeFType;

import javax.persistence.EntityManager;
import java.util.List;

public class PeFTypeDao implements Dao<PeFType> {

    private static EntityManager entityManager = SessionUtil.getEntityManager();

    @Override
    public PeFType get(int id) {
        entityManager.getTransaction().begin();
        PeFType peft = entityManager.find(PeFType.class, id);
        entityManager.getTransaction().commit();
        return peft;
    }

    @Override
    public List<PeFType> getAll() {
        entityManager.getTransaction().begin();
        List<PeFType> result = entityManager.createQuery("SELECT a FROM PeFType a", PeFType.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void save(PeFType peft) {
        entityManager.getTransaction().begin();
        entityManager.persist(peft);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(PeFType peft, String[] params) {

    }

    @Override
    public void delete(PeFType peft) {
        entityManager.getTransaction().begin();
        entityManager.remove(peft);
        entityManager.getTransaction().commit();
    }

    public PeFType getPeftByName(String name) {
        entityManager.getTransaction().begin();
        PeFType peft = entityManager.
                createQuery("SELECT a FROM PeFType a WHERE a.name = ?1", PeFType.class)
                .setParameter(1, name)
                .getSingleResult();

        entityManager.getTransaction().commit();
        return peft;
    }
}

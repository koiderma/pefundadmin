package DAL;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SessionUtil {

    static EntityManagerFactory emf = Persistence.createEntityManagerFactory("funddb");

    public static EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public static void closeFactory(){emf.close();};

}


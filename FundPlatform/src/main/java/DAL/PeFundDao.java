package DAL;

import Domain.PeFund;

import javax.persistence.EntityManager;
import java.util.List;

public class PeFundDao implements Dao<PeFund> {

    private static EntityManager entityManager = SessionUtil.getEntityManager();

    @Override
    public PeFund get(int id) {
        entityManager.getTransaction().begin();
        PeFund fund1 = entityManager.find(PeFund.class, id);
        entityManager.getTransaction().commit();
        return fund1;
    }

    @Override
    public List<PeFund> getAll() {
        entityManager.getTransaction().begin();
        List<PeFund> result = entityManager.createQuery("SELECT a FROM PeFund a", PeFund.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void save(PeFund peFund) {
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("INSERT INTO PeFund (ticker, name, startdate, investperioddate, enddate, peftype_id)  VALUES (?,?,?,?,?,?)")
                .setParameter(1, peFund.getTicker())
                .setParameter(2, peFund.getName())
                .setParameter(3, peFund.getStartDate())
                .setParameter(4, peFund.getInvestPeriodDate())
                .setParameter(5, peFund.getEndDate())
                .setParameter(6, peFund.getPefType().getId())
                .executeUpdate();

        entityManager.getTransaction().commit();
    }

    @Override
    public void update(PeFund peFund, String[] params) {
        entityManager.getTransaction().begin();
        entityManager.merge(peFund);
        entityManager.getTransaction().commit();
    }

    @Override
    public void delete(PeFund peFund) {
        entityManager.getTransaction().begin();
        entityManager.remove(peFund);
        entityManager.getTransaction().commit();
    }

    public PeFund getFundByName(String ticker) {
        entityManager.getTransaction().begin();
        PeFund fund = entityManager.
                createQuery("SELECT a FROM PeFund a WHERE a.ticker = ?1", PeFund.class)
                .setParameter(1, ticker)
                .getSingleResult();

        entityManager.getTransaction().commit();
        return fund;
    }
}
package DAL;


import Domain.Commitment;

import javax.persistence.EntityManager;
import java.util.List;

public class CommitmentDao implements Dao<Commitment> {

    private static EntityManager entityManager = SessionUtil.getEntityManager();

    @Override
    public Commitment get(int id) {
        entityManager.getTransaction().begin();
        Commitment com = entityManager.find(Commitment.class, id);
        entityManager.getTransaction().commit();
        return com;
    }

    @Override
    public List<Commitment> getAll() {
        entityManager.getTransaction().begin();
        List<Commitment> result = entityManager.createQuery("SELECT a FROM Commitment a", Commitment.class).getResultList();
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void save(Commitment com) {
        entityManager.getTransaction().begin();

        entityManager.createNativeQuery("INSERT INTO Commitment (pefund_id, pensionfund_id, commitmentsize) VALUES (?,?,?)")
                .setParameter(1, com.getPeFund().getId())
                .setParameter(2, com.getPensionFund().getId())
                .setParameter(3, com.getCommitmentSize())
                .executeUpdate();
        //entityManager.persist(com);
        entityManager.getTransaction().commit();
    }

    @Override
    public void update(Commitment com, String[] params) {

    }

    @Override
    public void delete(Commitment com) {
        entityManager.getTransaction().begin();
        entityManager.remove(com);
        entityManager.getTransaction().commit();
    }

    public Commitment getCommitmentByFunds(int pef_id, int pe_id) {
        entityManager.getTransaction().begin();
        Commitment com = entityManager.
                createQuery("SELECT a FROM Commitment a WHERE (a.peFund.id = ?1 and a.pensionFund.id =?2)", Commitment.class)
                .setParameter(1, pe_id)
                .setParameter(2, pef_id)
                .getSingleResult();

        entityManager.getTransaction().commit();
        return com;
    }

    public List<Commitment> getCommitmentByPensionFunds(int pef_id) {
        entityManager.getTransaction().begin();
        List<Commitment> result = entityManager.
                createQuery("SELECT a FROM Commitment a WHERE a.pensionFund.id = ?1 order by a.id asc", Commitment.class)
                .setParameter(1, pef_id)
                .getResultList();

        entityManager.getTransaction().commit();
        return result;
    }

    public List<Commitment> getCommitmentByPensionFundsBeforeDate(int pef_id, java.sql.Date cutOffDate) {
        entityManager.getTransaction().begin();
        List<Commitment> result = entityManager.
                createQuery("SELECT a FROM Commitment a WHERE a.pensionFund.id = ?1 and a.peFund.startDate <= ?2 order by a.id asc", Commitment.class)
                .setParameter(1, pef_id)
                .setParameter(2, cutOffDate)
                .getResultList();

        entityManager.getTransaction().commit();
        return result;
    }


}

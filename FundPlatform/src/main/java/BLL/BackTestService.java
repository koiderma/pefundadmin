package BLL;

import BLL.DTO.BackTestYearData;
import DAL.CashFlowDao;
import DAL.Dao;
import org.jboss.logging.Logger;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class BackTestService {

    static Logger log = Logger.getLogger(HistoricalDataService.class.getName());

    public List<BackTestYearData> getBackTestData(int id, int year) {

        if (year <= 2016) return null;

        int client_id = id;
        LocalDate backTestDate = LocalDate.of(year, 12, 31);

        HistoricalDataService hdService = new HistoricalDataService();
        hdService.setCalculationDayDate(backTestDate);
        FundCashFlowService fcfService = new FundCashFlowService();
        fcfService.setCutOffDate(java.sql.Date.valueOf(backTestDate));
        FundCfModel forecastModel = new FundCfModel();
        fcfService.setAllAggregate(forecastModel);

        fcfService.setGrowthRate(hdService.getGrowthParameter(client_id));
        fcfService.setRcp1(hdService.getOutFlowParameterFirstYear(client_id));
        fcfService.setRcp2(hdService.getOutFlowParameterSecondYear(client_id));
        fcfService.setRcpNext(hdService.getOutFlowParameterOtherYears(client_id));
        fcfService.setBow(hdService.getDistributionParameter(client_id));

        fcfService.calculateClientCashFlowEstimates(client_id);
        forecastModel = fcfService.getAllAggregate();

        long fundLifeYears = ChronoUnit.YEARS.between(backTestDate, LocalDate.now());
        List<BackTestYearData> allDataList = new ArrayList<>();
        Dao cashFlowDao = new CashFlowDao();
        LocalDate startDate = backTestDate;
        double mapeSumOutFlow = 0;
        double mapeSumInFlow = 0;
        int mapeCount = 0;

        for (int i = 0; i < fundLifeYears; i++) {

            BackTestYearData contributionData = new BackTestYearData();
            contributionData.setYear(year + 1 + i);
            contributionData.setEstimatedAmount(forecastModel.cashOutFlows.get(i));
            contributionData.setCashFlowType("Contribution");
            int cfOutActual = ((CashFlowDao) cashFlowDao).getCashOutFlowsBetweenDatesByClient(client_id, java.sql.Date.valueOf(backTestDate), java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(startDate.plusYears(1)));
            contributionData.setActualAmount(cfOutActual);
            allDataList.add(contributionData);

            BackTestYearData distributionData = new BackTestYearData();
            distributionData.setYear(year + 1 + i);
            distributionData.setEstimatedAmount(forecastModel.cashInFlows.get(i));
            distributionData.setCashFlowType("Distribution");
            int cfInActual = ((CashFlowDao) cashFlowDao).getCashInFlowsBetweenDatesByClient(client_id, java.sql.Date.valueOf(backTestDate), java.sql.Date.valueOf(startDate), java.sql.Date.valueOf(startDate.plusYears(1)));
            distributionData.setActualAmount(cfInActual);
            allDataList.add(distributionData);

            double errorPercentageOutFlow = ((double) cfOutActual - (double) forecastModel.cashOutFlows.get(i)) / (double) cfOutActual;
            double errorPercentageInFlow = ((double) cfInActual - (double) forecastModel.cashInFlows.get(i)) / (double) cfInActual;
            mapeSumOutFlow += Math.abs(errorPercentageOutFlow);
            mapeSumInFlow += Math.abs(errorPercentageInFlow);
            mapeCount++;
            startDate = startDate.plusYears(1);
        }

        log.info("MAPE outflow: " + mapeSumOutFlow / mapeCount * 100 + "%");
        log.info("MAPE inflow: " + mapeSumInFlow / mapeCount * 100 + "%");
        return allDataList;
    }

}

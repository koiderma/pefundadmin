package BLL;

public final class AppConstants {

    private AppConstants() {
    }

    public static final double SMOOTHING_PARAMETER = 0.25;
    public static final double GROWTH_RATE = 0.12;
    public static final double DISTRIBUTION_PARAMETER = 1.26;
    public static final double VALID_R2 = 0.35;
    public static final double FIRST_YEAR_DRAWDOWN = 0.25;
    public static final double SECOND_YEAR_DRAWDOWN = 0.33;
    public static final double OTHER_YEAR_DRAWDOWN = 0.50;
    public static final String AGGREGATE_FUND_NAME = "All Funds";
    public static final double DISTRIBUTION_RATE_NEAR_NULL = 0.0001;

}

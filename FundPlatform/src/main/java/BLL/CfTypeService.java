package BLL;

import DAL.CfTypeDao;
import DAL.Dao;
import Domain.CfType;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class CfTypeService {

    static Logger log = Logger.getLogger(CfTypeService.class.getName());

    public List<BLL.DTO.CfType> getCashFlowTypes() {

        List<BLL.DTO.CfType> returnCFList = new ArrayList<>();
        Dao cfDao = new CfTypeDao();
        List<CfType> cfList = cfDao.getAll();
        for (Domain.CfType cft : cfList) {
            returnCFList.add(new BLL.DTO.CfType(cft.getId(), cft.getName()));
        }
        log.info(returnCFList);
        return returnCFList;
    }

}


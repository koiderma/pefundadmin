package BLL.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PeFund {

    private int id;
    private String ticker;
    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "et", timezone = "Europe/Helsinki")
    private java.sql.Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "et", timezone = "Europe/Helsinki")
    private java.sql.Date investPeriodDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "et", timezone = "Europe/Helsinki")
    private java.sql.Date endDate;

    private String peFundType;
}

package BLL.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Nav {

    private int id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "et", timezone = "Europe/Helsinki")
    private java.sql.Date navDate;
    private int capitalSize;
    private String pensionFundName;
    private String peFundName;

}

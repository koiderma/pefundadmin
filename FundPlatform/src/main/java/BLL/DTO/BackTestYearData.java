package BLL.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BackTestYearData {

    private int year;
    private String cashFlowType;
    private int estimatedAmount;
    private int actualAmount;

}

package BLL.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Commitment {

    private int id;
    private int commitmentSize;
    private String pensionFundName;
    private String peFundName;
}

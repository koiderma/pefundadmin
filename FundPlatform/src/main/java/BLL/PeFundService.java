package BLL;

import BLL.DTO.PeFund;
import DAL.Dao;
import DAL.PeFTypeDao;
import DAL.PeFundDao;
import Domain.PeFType;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class PeFundService {

    static Logger log = Logger.getLogger(PeFundService.class.getName());

    public List<PeFund> getAllFunds() {

        List<PeFund> fundList = new ArrayList<>();
        Dao peFundDao = new PeFundDao();
        List<Domain.PeFund> pfList = peFundDao.getAll();

        for (Domain.PeFund pef : pfList) {
            fundList.add(new PeFund(pef.getId(), pef.getTicker(), pef.getName(),
                    pef.getStartDate(), pef.getInvestPeriodDate(), pef.getEndDate(), pef.getPefType().getName()));
        }

        log.info(fundList);
        return fundList;
    }

    public PeFund getFund(int id) {
        Dao peFundDao = new PeFundDao();
        Domain.PeFund pef = ((PeFundDao) peFundDao).get(id);
        PeFund resultFund = new PeFund(pef.getId(), pef.getTicker(), pef.getName(),
                pef.getStartDate(), pef.getInvestPeriodDate(), pef.getEndDate(), pef.getPefType().getName());

        log.info(resultFund);
        return resultFund;
    }

    public void addPeFund(PeFund peFund) {
        Dao peFundDao = new PeFundDao();
        Domain.PeFund newPeFund = new Domain.PeFund();
        newPeFund.setName(peFund.getName());
        newPeFund.setTicker(peFund.getTicker());
        newPeFund.setStartDate(peFund.getStartDate());
        newPeFund.setInvestPeriodDate(peFund.getInvestPeriodDate());
        newPeFund.setEndDate(peFund.getEndDate());

        Dao peftDao = new PeFTypeDao();
        PeFType peft = ((PeFTypeDao) peftDao).getPeftByName(peFund.getPeFundType());
        newPeFund.setPefType(peft);

        peFundDao.save(newPeFund);
    }

    public void deletePeFund(int id) {
        Dao peFundDao = new PeFundDao();
        peFundDao.delete(peFundDao.get(id));
    }

    public void updatePeFund(PeFund peFund, int id) {

        Dao peFundDao = new PeFundDao();
        Domain.PeFund oldFund = ((PeFundDao) peFundDao).get(id);
        oldFund.setTicker(peFund.getTicker());
        oldFund.setName(peFund.getName());
        oldFund.setStartDate(peFund.getStartDate());
        oldFund.setInvestPeriodDate(peFund.getInvestPeriodDate());
        oldFund.setEndDate(peFund.getEndDate());
        peFundDao.update(oldFund, null);
    }


}

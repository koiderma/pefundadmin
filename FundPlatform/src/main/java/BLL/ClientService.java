package BLL;


import BLL.DTO.Client;
import DAL.Dao;
import DAL.PensionFundDao;
import Domain.PensionFund;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class ClientService {

    static Logger log = Logger.getLogger(ClientService.class.getName());

    public List<Client> getAllClients() {

        List<Client> clientList = new ArrayList<>();
        Dao pensionFundDao = new PensionFundDao();
        List<PensionFund> pfList = pensionFundDao.getAll();

        for (PensionFund pef : pfList) {
            clientList.add(new Client(pef.getId(), pef.getName()));
        }

        log.info(clientList);
        return clientList;
    }

}

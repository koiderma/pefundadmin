package BLL;

import DAL.*;
import Domain.Commitment;
import lombok.Setter;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.jboss.logging.Logger;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class HistoricalDataService {

    static Logger log = Logger.getLogger(HistoricalDataService.class.getName());
    @Setter
    LocalDate calculationDayDate = LocalDate.now();
    @Setter
    NavStatementDao navDao = new NavStatementDao();
    @Setter
    CashFlowDao cfDao = new CashFlowDao();

    public double getGrowthParameter(int client_id) {

        List navList = new ArrayList();
        List cfList = new ArrayList();
        List growthList = new ArrayList();

        getFundCashFlowsAndNavLists(client_id, navList, cfList);
        calculateGrowthRatesByYearList(navList, cfList, growthList);
        Double growthRate = exponentialSmoothing(growthList);
        log.info("Total growth: " + growthRate);

        return validateGrowthRate(growthRate);
    }

    public double validateGrowthRate(Double growthRate) {
        if (growthRate < 0.25 && growthRate > 0.05) {
            return (growthRate);
        } else {
            return AppConstants.GROWTH_RATE;
        }
    }

    public void calculateGrowthRatesByYearList(List navList, List cfList, List growthList) {

        for (int i = 0; i < navList.size() - 1; i++) {

            double growth = ((double) (int) navList.get(i) + (int) cfList.get(i)) / (int) navList.get(i + 1) - 1;
            growthList.add(growth);
        }
        growthList.add(AppConstants.GROWTH_RATE);
    }

    public void getFundCashFlowsAndNavLists(int client_id, List navList, List cfList) {

        LocalDate endDate = getGrowthCalculationPeriodEndDate();

        for (int i = 0; i < 10; i++) {
            LocalDate startDate = endDate.minusYears(1).plusDays(1);
            Date eDate = Date.valueOf(endDate);
            Date sDate = Date.valueOf(startDate);

            int totalSize = navDao.getClientHistoricalNAV(client_id, eDate);
            int totalCashFlow = cfDao.getCashFlowsBetweenDates(client_id, sDate, eDate);

            if (totalSize == -1) break;
            navList.add(totalSize);
            cfList.add(totalCashFlow);
            endDate = endDate.minusYears(1);
        }
    }

    public double exponentialSmoothing(List valueList) {
        double sumOfParam = 0;
        double sumOfValues = 0;
        for (int i = 0; i < valueList.size(); i++) {
            double param = AppConstants.SMOOTHING_PARAMETER * Math.pow((1 - AppConstants.SMOOTHING_PARAMETER), i);
            sumOfParam += param;
            double value = param * (double) valueList.get(i);
            sumOfValues += value;
        }
        return sumOfValues / sumOfParam;
    }

    public LocalDate getGrowthCalculationPeriodEndDate() {
        LocalDate dateNow = calculationDayDate;
        LocalDate endDate;
        if (dateNow.getMonthValue() >= 4) {
            endDate = LocalDate.of(dateNow.getYear() - 1, 12, 31);
        } else {
            endDate = LocalDate.of(dateNow.getYear() - 2, 12, 31);
        }
        return endDate;
    }

    public LocalDate getFlowParameterCutOffDate() {
        return calculationDayDate;
    }

    public LocalDate getInFlowParameterCutOffDate() {
        LocalDate dateNow = calculationDayDate;
        LocalDate endDate;
        if (dateNow.getMonthValue() == 12 && dateNow.getDayOfMonth() == 31) {
            endDate = dateNow;
        } else {
            endDate = LocalDate.of(dateNow.getYear() - 1, 12, 31);
        }
        return endDate;
    }

    public double getOutFlowParameterFirstYear(int client_id) {

        Dao commitmentDao = new CommitmentDao();
        Map<Integer, List<Double>> firstYear = new HashMap();
        LocalDate cutOffDate = getFlowParameterCutOffDate();

        List<Domain.Commitment> commitments = ((CommitmentDao) commitmentDao).getCommitmentByPensionFunds(client_id);
        for (Domain.Commitment com : commitments) {

            getFirstYearOutFlowAllData(client_id, firstYear, cutOffDate, com);
        }

        Map firstYearAverages = calculateMapAverages(firstYear);
        double firstYearResult = getExponentialSmoothedAverage(firstYearAverages);
        log.info("First year drawdowns: " + firstYearResult);

        return validateFirstYearParameter(firstYearResult);
    }

    public double validateFirstYearParameter(double firstYearResult) {
        if (firstYearResult > 0.05 && firstYearResult < 0.35) {
            return firstYearResult;
        } else {
            return AppConstants.FIRST_YEAR_DRAWDOWN;
        }
    }

    public void getFirstYearOutFlowAllData(int client_id, Map<Integer, List<Double>> firstYear, LocalDate cutOffDate, Commitment com) {

        LocalDate startDate = com.getPeFund().getStartDate().toLocalDate();
        LocalDate endDate = com.getPeFund().getStartDate().toLocalDate().plusYears(1).minusDays(1);

        if (endDate.isBefore(cutOffDate)) {
            int totalFlow = getTotalFlow(client_id, com, startDate, endDate);
            if (totalFlow < 0) {
                double firstYearPercent = (double) totalFlow / com.getCommitmentSize() * -1;

                if (!firstYear.containsKey(endDate.getYear())) {
                    firstYear.put(endDate.getYear(), new ArrayList());
                }
                firstYear.get(endDate.getYear()).add(firstYearPercent);
            }
        }
    }

    public double getOutFlowParameterSecondYear(int client_id) {

        Dao commitmentDao = new CommitmentDao();
        Map<Integer, List<Double>> secondYear = new HashMap();
        LocalDate cutOffDate = getFlowParameterCutOffDate();

        List<Domain.Commitment> result = ((CommitmentDao) commitmentDao).getCommitmentByPensionFunds(client_id);
        for (Domain.Commitment com : result) {

            getSecondYearOutFlowAllData(client_id, secondYear, cutOffDate, com);
        }

        Map secondYearAverages = calculateMapAverages(secondYear);
        double secondYearResult = getExponentialSmoothedAverage(secondYearAverages);
        log.info("Second year drawdowns: " + secondYearResult);
        return validateSecondYearOutFlowParameter(secondYearResult);
    }

    public void getSecondYearOutFlowAllData(int client_id, Map<Integer, List<Double>> secondYear, LocalDate cutOffDate, Commitment com) {
        LocalDate startDate = com.getPeFund().getStartDate().toLocalDate();
        LocalDate endDate = com.getPeFund().getStartDate().toLocalDate().plusYears(1).minusDays(1);
        int pic = com.getCommitmentSize();

        if (endDate.isBefore(cutOffDate)) {
            int totalFlow = getTotalFlow(client_id, com, startDate, endDate);
            pic += totalFlow;
        }

        if (pic != com.getCommitmentSize()) {
            endDate = endDate.plusYears(1);
            startDate = startDate.plusYears(1);

            if (endDate.isBefore(cutOffDate) && pic > 0) {
                int totalFlow = getTotalFlow(client_id, com, startDate, endDate);
                double secondYearPercent = (double) totalFlow / pic * -1;

                if (!secondYear.containsKey(endDate.getYear())) {
                    secondYear.put(endDate.getYear(), new ArrayList());
                }
                secondYear.get(endDate.getYear()).add(secondYearPercent);
            }
        }

    }

    public double validateSecondYearOutFlowParameter(double secondYearResult) {
        if (secondYearResult < 0.45 && secondYearResult > 0.10) {
            return secondYearResult;
        } else {
            return AppConstants.SECOND_YEAR_DRAWDOWN;
        }
    }

    public double getOutFlowParameterOtherYears(int client_id) {

        Dao commitmentDao = new CommitmentDao();
        Map<Integer, List<Double>> nextYear = new HashMap();
        LocalDate cutOffDate = getFlowParameterCutOffDate();

        List<Domain.Commitment> result = ((CommitmentDao) commitmentDao).getCommitmentByPensionFunds(client_id);
        for (Domain.Commitment com : result) {

            getOtherYearOutFlowAllData(client_id, nextYear, cutOffDate, com);
        }

        Map nextYearAverages = calculateMapAverages(nextYear);
        double nextYearResult = getExponentialSmoothedAverage(nextYearAverages);
        log.info("Final year drawdowns: " + nextYearResult);
        return validateOtherYearOutFlowParameter(nextYearResult);

    }

    public void getOtherYearOutFlowAllData(int client_id, Map<Integer, List<Double>> nextYear, LocalDate cutOffDate, Commitment com) {
        LocalDate startDate = com.getPeFund().getStartDate().toLocalDate();
        LocalDate endDate = com.getPeFund().getStartDate().toLocalDate().plusYears(1).minusDays(1);
        int pic = com.getCommitmentSize();

        if (endDate.isBefore(cutOffDate)) {
            int totalFlow = getTotalFlow(client_id, com, startDate, endDate);
            pic += totalFlow;
        }

        endDate = endDate.plusYears(1);
        startDate = startDate.plusYears(1);

        if (endDate.isBefore(cutOffDate) && pic > 0) {
            int totalFlow = getTotalFlow(client_id, com, startDate, endDate);
            pic += totalFlow;
        }

        endDate = endDate.plusYears(1);
        startDate = startDate.plusYears(1);
        int year = 3;
        while (endDate.isBefore(cutOffDate) && year <= 5 && pic > 0) {
            int totalFlow = getTotalFlow(client_id, com, startDate, endDate);
            double nextYearPercent = (double) totalFlow / pic * -1;
            if (!nextYear.containsKey(endDate.getYear())) {
                nextYear.put(endDate.getYear(), new ArrayList());
            }
            nextYear.get(endDate.getYear()).add(nextYearPercent);

            pic += totalFlow;
            endDate = endDate.plusYears(1);
            startDate = startDate.plusYears(1);
            year++;
        }
    }

    public double validateOtherYearOutFlowParameter(double nextYearResult) {
        if (nextYearResult < 0.60 && nextYearResult > 0.15) {
            return nextYearResult;
        } else {
            return AppConstants.OTHER_YEAR_DRAWDOWN;
        }
    }

    public int getTotalFlow(int client_id, Commitment com, LocalDate startDate, LocalDate endDate) {
        Date eDate = Date.valueOf(endDate);
        Date sDate = Date.valueOf(startDate);
        Dao cfDao = new CashFlowDao();
        return ((CashFlowDao) cfDao).getCashOutFlowsBetweenDates(client_id, com.getPeFund().getId(),
                sDate, eDate);
    }

    public double getExponentialSmoothedAverage(Map<Integer, Double> factorYear) {

        List<Integer> keys = new ArrayList<>(factorYear.keySet());
        Collections.sort(keys, Collections.reverseOrder());

        double sumOfParam = 0;
        double sumOfValues = 0;
        for (int i = 0; i < factorYear.size(); i++) {

            double param = AppConstants.SMOOTHING_PARAMETER * Math.pow((1 - AppConstants.SMOOTHING_PARAMETER), i);
            sumOfParam += param;
            double value = param * (double) factorYear.get(keys.get(i));
            sumOfValues += value;
        }

        return sumOfValues / sumOfParam;
    }

    public Map calculateMapAverages(Map<Integer, List<Double>> firstYear) {
        Map<Integer, Double> returnMap = new HashMap();
        for (Map.Entry<Integer, List<Double>> entry : firstYear.entrySet()) {
            List<Double> valueList = entry.getValue();
            double sum = 0;
            for (int i = 0; i < valueList.size(); i++) {
                sum += valueList.get(i);
            }
            double sumf = sum / valueList.size();
            returnMap.put(entry.getKey(), sumf);
        }

        return returnMap;
    }

    public double getDistributionParameter(int client_id) {

        Dao commitmentDao = new CommitmentDao();
        Double growth = getGrowthParameter(client_id);
        SimpleRegression regression = new SimpleRegression();

        List<Domain.Commitment> result = ((CommitmentDao) commitmentDao).getCommitmentByPensionFunds(client_id);
        for (Domain.Commitment com : result) {

            getDistributionParameterRegressionData(client_id, growth, regression, com);
        }

        log.info("regression slope: " + regression.getSlope());
        log.info("regression R2: " + regression.getRSquare());

        if (Double.isNaN(regression.getRSquare()) || regression.getRSquare() < AppConstants.VALID_R2) {
            return AppConstants.DISTRIBUTION_PARAMETER;
        } else return regression.getSlope();
    }

    public void getDistributionParameterRegressionData(int client_id, Double growth, SimpleRegression regression, Commitment com) {
        LocalDate fundstartDate = com.getPeFund().getStartDate().toLocalDate();
        LocalDate fundendDate = com.getPeFund().getEndDate().toLocalDate();
        long fundLifeDays = ChronoUnit.DAYS.between(fundstartDate, fundendDate);
        LocalDate endDate = getInFlowParameterCutOffDate();

        for (int i = 0; i < 6; i++) {

            LocalDate startDate = endDate.minusYears(1);
            Date sDate = Date.valueOf(startDate.plusDays(1));
            Date sDateNav = Date.valueOf(startDate);
            Date eDate = Date.valueOf(endDate);

            int totalFlow = cfDao.getCashInFlowsBetweenDates(client_id,
                    com.getPeFund().getId(), sDate, eDate);
            int navSize = navDao.getNavByFundsAndDate(com.getPensionFund().getId(),
                    com.getPeFund().getId(), sDateNav);

            long fundWorkingDays = ChronoUnit.DAYS.between(fundstartDate, startDate);
            if (fundWorkingDays < 0) break;

            if (navSize != -1) {

                double distributionRate = totalFlow / (navSize * (1 + growth));
                double tL = (double) fundWorkingDays / fundLifeDays;

                if (distributionRate != 0) {
                    regression.addData(Math.log(tL), Math.log(distributionRate));
                } else {
                    regression.addData(Math.log(tL), Math.log(AppConstants.DISTRIBUTION_RATE_NEAR_NULL));
                }
            }
            endDate = endDate.minusYears(1);
        }
    }

}

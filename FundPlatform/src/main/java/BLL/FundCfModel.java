package BLL;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FundCfModel {

    private String clientFundName;
    private String peFundName;
    private int latestNAV;
    List<Integer> cashOutFlows = new ArrayList<Integer>();
    List<Integer> cashInFlows = new ArrayList<Integer>();
    List<Integer> navs = new ArrayList<Integer>();

    @Override
    public String toString() {
        return "Fund: " + peFundName + "\n" + "Client: " + clientFundName + "\n" + "NAV: " + latestNAV + "\n" +
                "outflows: " + cashOutFlows + "\n" +
                "inflows: " + cashInFlows + "\n" +
                "NAVs: " + navs;
    }
}

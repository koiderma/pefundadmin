package BLL;

import BLL.DTO.CFlow;
import DAL.*;
import Domain.CashFlow;
import Domain.CfType;
import Domain.PeFund;
import Domain.PensionFund;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class CashFlowService {

    static Logger log = Logger.getLogger(CashFlowService.class.getName());

    public List<CFlow> getCashFlowByPeFund(int pe_id) {

        List<CFlow> returnCFList = new ArrayList<>();
        Dao cfDao = new CashFlowDao();
        List<Domain.CashFlow> cfList = ((CashFlowDao) cfDao).getCashFlowByPeFunds(pe_id);
        for (Domain.CashFlow cf : cfList) {
            returnCFList.add(new CFlow(cf.getId(), cf.getPaymentDate(), cf.getCashFlow(),
                    cf.getPensionFund().getName(), cf.getPeFund().getTicker(), cf.getCfType().getName()));
        }
        log.info(returnCFList);
        return returnCFList;
    }

    public CFlow getCashFlow(int id) {
        Dao cfDao = new CashFlowDao();
        CashFlow cf = ((CashFlowDao) cfDao).get(id);
        CFlow resultCf = new CFlow(cf.getId(), cf.getPaymentDate(), cf.getCashFlow(),
                cf.getPensionFund().getName(), cf.getPeFund().getTicker(), cf.getCfType().getName());
        log.info(resultCf);
        return resultCf;
    }

    public void addCashFlow(CFlow cf) {
        Dao cfDao = new CashFlowDao();
        Domain.CashFlow newCf = new Domain.CashFlow();
        newCf.setCashFlow(cf.getCashFlow());
        newCf.setPaymentDate(cf.getPaymentDate());

        Dao clienDao = new PensionFundDao();
        PensionFund client = ((PensionFundDao) clienDao).getFundByTicker(cf.getPensionFundName());
        newCf.setPensionFund(client);

        Dao fundDao = new PeFundDao();
        PeFund fund = ((PeFundDao) fundDao).getFundByName(cf.getPeFundName());
        newCf.setPeFund(fund);

        Dao cftDao = new CfTypeDao();
        CfType cft = ((CfTypeDao) cftDao).getCftByName(cf.getCashFlowType());
        newCf.setCfType(cft);

        cfDao.save(newCf);
    }

    public void deleteCashFlow(int id) {
        Dao cfDao = new CashFlowDao();
        cfDao.delete(cfDao.get(id));
    }

    public void updateCashFlow(CFlow cf, int id) {
        Dao cfDao = new CashFlowDao();
        CashFlow oldCf = ((CashFlowDao) cfDao).get(id);
        oldCf.setPaymentDate(cf.getPaymentDate());
        oldCf.setCashFlow(cf.getCashFlow());
        cfDao.update(oldCf, null);
    }


}

package BLL;

import DAL.*;
import Domain.*;
import Domain.Commitment;
import lombok.Getter;
import lombok.Setter;
import org.jboss.logging.Logger;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FundCashFlowService {

    static Logger log = Logger.getLogger(FundCashFlowService.class.getName());
    @Setter
    private double growthRate;
    @Setter
    private double bow;
    @Setter
    private double rcp1;
    @Setter
    private double rcp2;
    @Setter
    private double rcpNext;
    @Setter
    private java.sql.Date cutOffDate;
    @Getter
    @Setter
    private FundCfModel allAggregate;
    @Setter
    Dao commitmentDao = new CommitmentDao();

    public FundCfModel getFundCashFlowsByPensionFund(int pef_id) {

        getModelParameters(pef_id);
        setCutOffDate();
        Dao pensionFundDao = new PensionFundDao();
        PensionFund pensionFund = (PensionFund) pensionFundDao.get(pef_id);

        allAggregate = new FundCfModel();
        allAggregate.setClientFundName(pensionFund.getName());
        allAggregate.setPeFundName(AppConstants.AGGREGATE_FUND_NAME);
        calculateClientCashFlowEstimates(pef_id);

        log.info(allAggregate);
        return allAggregate;
    }

    public void calculateClientCashFlowEstimates(int pef_id) {

        List<Domain.Commitment> comList = ((CommitmentDao) commitmentDao).getCommitmentByPensionFundsBeforeDate(pef_id, cutOffDate);

        for (Commitment com : comList) {

            FundCfModel clientFund = getPeFundCashFlowsByPensionFund(pef_id, com.getPeFund(), com);
            allAggregate.setLatestNAV(allAggregate.getLatestNAV() + clientFund.getLatestNAV());

            for (int i = 0; i < clientFund.cashOutFlows.size(); i++) {
                if (allAggregate.cashOutFlows.size() <= i) {
                    allAggregate.cashOutFlows.add(clientFund.cashOutFlows.get(i));
                    allAggregate.cashInFlows.add(clientFund.cashInFlows.get(i));
                    allAggregate.navs.add(clientFund.navs.get(i));
                } else {
                    allAggregate.cashOutFlows.set(i, clientFund.cashOutFlows.get(i) + allAggregate.cashOutFlows.get(i));
                    allAggregate.cashInFlows.set(i, clientFund.cashInFlows.get(i) + allAggregate.cashInFlows.get(i));
                    allAggregate.navs.set(i, clientFund.navs.get(i) + allAggregate.navs.get(i));
                }
            }
        }
    }

    public void getModelParameters(int pef_id) {
        growthRate = new HistoricalDataService().getGrowthParameter(pef_id);
        log.info("Growth rate recieved " + growthRate);
        bow = new HistoricalDataService().getDistributionParameter(pef_id);
        log.info("bow recieved " + bow);
        rcp1 = new HistoricalDataService().getOutFlowParameterFirstYear(pef_id);
        log.info("rcp1 recieved " + rcp1);
        rcp2 = new HistoricalDataService().getOutFlowParameterSecondYear(pef_id);
        log.info("rcp2 received " + rcp2);
        rcpNext = new HistoricalDataService().getOutFlowParameterOtherYears(pef_id);
        log.info("rcp3 receied " + rcpNext);
    }

    public void setCutOffDate() {
        cutOffDate = java.sql.Date.valueOf(LocalDate.now());
    }

    public FundCfModel getPeFundCashFlowsByPensionFund(int pf_id, Domain.PeFund peFund, Domain.Commitment com) {

        FundCfModel cfModel = new FundCfModel();

        Dao navStatementDao = new NavStatementDao();
        NavStatement nav = ((NavStatementDao) navStatementDao).getLatestNavByFundsBeforeDate(pf_id, peFund.getId(), cutOffDate);

        Dao cashFlowDao = new CashFlowDao();
        List<CashFlow> flows = ((CashFlowDao) cashFlowDao).getCashFlowByFundsBeforeDate(pf_id, peFund.getId(), cutOffDate);

        long daysFundWorking = getDaysFundWorking(peFund);
        long fundMaturity = getFundMaturity(peFund, com, nav.getCapitalSize(), daysFundWorking);
        int payInCashFlow = getPayInCashFlow(flows);

        cfModel.setPeFundName(com.getPeFund().getTicker());
        cfModel.setClientFundName(com.getPensionFund().getName());
        calculatePeFundOutFlowForecast(cfModel, daysFundWorking, fundMaturity, com.getCommitmentSize(), payInCashFlow);
        getPeFundNAVInflowByPensionFund(cfModel, daysFundWorking, fundMaturity, nav, flows, com);

        log.info(cfModel);
        return cfModel;
    }

    public long getDaysFundWorking(Domain.PeFund peFund) {

        return ChronoUnit.DAYS.between(peFund.getStartDate().toLocalDate(), cutOffDate.toLocalDate());

    }

    public long getFundMaturity(Domain.PeFund peFund, Domain.Commitment com, double navValue, long daysFundWorking) {

        long fundMaturity = ChronoUnit.DAYS.between(peFund.getStartDate().toLocalDate(), peFund.getEndDate().toLocalDate());

        if ((fundMaturity - daysFundWorking) <= 180 && (fundMaturity - daysFundWorking) >= 0 && (navValue / (double) com.getCommitmentSize()) > 0.05) {
            fundMaturity = fundMaturity + 365;
        }

        if ((fundMaturity - daysFundWorking) <= 365 && (fundMaturity - daysFundWorking) > 180 && (navValue / (double) com.getCommitmentSize()) > 0.15) {
            fundMaturity = fundMaturity + 365;
        }

        if (fundMaturity < daysFundWorking) {
            fundMaturity = fundMaturity + 365;
        }

        return fundMaturity;
    }

    public FundCfModel calculatePeFundOutFlowForecast(FundCfModel cfModel, long daysFundWorking, long fundMaturity, int commitmentSize, int payInCashFlow) {

        double rcp = 0.0;
        for (long i = daysFundWorking; i < fundMaturity; i = i + 365) {
            if (i < 365) {
                rcp = ((365 - i) * rcp1 + i * rcp2) / 365;
            } else if (i >= 365 && i < 730) {
                rcp = ((730 - i) * rcp2 + (i - 365) * rcpNext) / 365;
            } else if (i >= 730) {
                rcp = rcpNext;
            }
            int cashFlowForecast = (int) Math.round(rcp * (commitmentSize - payInCashFlow));
            payInCashFlow += cashFlowForecast;
            cfModel.cashOutFlows.add(-cashFlowForecast);
        }

        return cfModel;
    }

    public int getPayInCashFlow(List<CashFlow> flows) {
        int payInCashFlow = 0;

        for (CashFlow cf : flows) {
            if ((cf.getCfType().getId() == 0 || cf.getCfType().getId() == 2 || cf.getCfType().getId() == 3))
                payInCashFlow -= cf.getCashFlow();
        }
        return payInCashFlow;
    }

    public int calculateCashFlowAfterNAV(List<CashFlow> flows, Date navDate) {

        int cashFlow = 0;
        if (navDate == null) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.YEAR, 2010);
            cal.set(Calendar.MONTH, Calendar.JANUARY);
            cal.set(Calendar.DAY_OF_MONTH, 1);
            navDate = cal.getTime();
        }

        for (CashFlow cf : flows) {
            if ((cf.getCfType().getId() == 0 || cf.getCfType().getId() == 1 || cf.getCfType().getId() == 2 || cf.getCfType().getId() == 3) && cf.getPaymentDate().after(navDate)) {
                cashFlow -= cf.getCashFlow();
            }
        }
        return cashFlow;
    }

    public FundCfModel getPeFundNAVInflowByPensionFund(FundCfModel cfModel, long daysFundWorking, long fundMaturity, NavStatement nav, List<CashFlow> flows, Domain.Commitment com) {

        double yield = 0.0;
        double useBow = bow;
        int cashFlowAfterNAV = calculateCashFlowAfterNAV(flows, nav.getNavDate());
        double navValue = nav.getCapitalSize();
        navValue += cashFlowAfterNAV;
        cfModel.setLatestNAV((int) navValue);

        if (((fundMaturity - daysFundWorking) <= 2 * 365) && (navValue / (double) com.getCommitmentSize()) > 0.40) {
            useBow = 3.5;
        }

        int j = 0;
        for (long i = daysFundWorking; i < fundMaturity; i = i + 365) {
            double distribution;
            double rd = Math.max(yield, Math.pow(((double) i / (double) fundMaturity), useBow));
            if ((fundMaturity - i) < 365) {
                distribution = navValue * (1 + growthRate) - cfModel.cashOutFlows.get(j);
            } else {
                distribution = rd * (navValue * (1 + growthRate));
            }
            navValue = navValue * (1 + growthRate) - distribution - cfModel.cashOutFlows.get(j);
            cfModel.cashInFlows.add((int) distribution);
            cfModel.navs.add((int) navValue);
            j++;
        }

        return cfModel;
    }

}

package BLL;

import BLL.DTO.PefType;
import DAL.Dao;
import DAL.PeFTypeDao;
import Domain.PeFType;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class PefTypeService {

    static Logger log = Logger.getLogger(PefTypeService.class.getName());

    public List<PefType> getPeFundTypes() {

        List<PefType> returnPefList = new ArrayList<>();
        Dao pefDao = new PeFTypeDao();
        List<PeFType> pefList = pefDao.getAll();
        for (PeFType peft : pefList) {
            returnPefList.add(new PefType(peft.getId(), peft.getName()));
        }
        log.info(returnPefList);
        return returnPefList;
    }
}

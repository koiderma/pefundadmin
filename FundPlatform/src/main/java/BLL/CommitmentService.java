package BLL;

import BLL.DTO.Commitment;
import DAL.CommitmentDao;
import DAL.Dao;
import DAL.PeFundDao;
import DAL.PensionFundDao;
import Domain.PeFund;
import Domain.PensionFund;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class CommitmentService {

    static Logger log = Logger.getLogger(CommitmentService.class.getName());

    public List<Commitment> getCommitmentsByFund(int pef_id) {

        List<Commitment> returnComList = new ArrayList<>();
        Dao comDao = new CommitmentDao();
        List<Domain.Commitment> comList = ((CommitmentDao) comDao).getCommitmentByPensionFunds(pef_id);

        for (Domain.Commitment com : comList) {
            returnComList.add(new Commitment(com.getId(), com.getCommitmentSize(),
                    com.getPensionFund().getName(), com.getPeFund().getTicker()));
        }

        log.info(returnComList);
        return returnComList;
    }

    public Commitment getCommitment(int id) {
        Dao comDao = new CommitmentDao();
        Domain.Commitment com = ((CommitmentDao) comDao).get(id);
        Commitment resultCom = new Commitment(com.getId(), com.getCommitmentSize(), com.getPensionFund().getName(), com.getPeFund().getTicker());
        System.out.println(resultCom);
        log.info(resultCom);
        return resultCom;
    }

    public void addCommitment(Commitment com) {
        Dao comdDao = new CommitmentDao();
        Domain.Commitment newCom = new Domain.Commitment();
        newCom.setCommitmentSize(com.getCommitmentSize());

        Dao clienDao = new PensionFundDao();
        PensionFund client = ((PensionFundDao) clienDao).getFundByTicker(com.getPensionFundName());
        newCom.setPensionFund(client);

        Dao fundDao = new PeFundDao();
        PeFund fund = ((PeFundDao) fundDao).getFundByName(com.getPeFundName());
        newCom.setPeFund(fund);

        comdDao.save(newCom);
    }

    public void deleteCommitment(int id) {
        Dao comDao = new CommitmentDao();
        comDao.delete(comDao.get(id));
    }

    public void updateCommitment(Commitment com, int id) {

        Dao comDao = new CommitmentDao();
        Domain.Commitment oldCom = ((CommitmentDao) comDao).get(id);
        oldCom.setCommitmentSize(com.getCommitmentSize());
        comDao.save(oldCom);
    }

}

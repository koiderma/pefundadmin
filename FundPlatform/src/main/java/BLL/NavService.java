package BLL;

import BLL.DTO.Nav;
import DAL.Dao;
import DAL.NavStatementDao;
import DAL.PeFundDao;
import DAL.PensionFundDao;
import Domain.PeFund;
import Domain.PensionFund;
import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class NavService {

    static Logger log = Logger.getLogger(NavService.class.getName());

    public List<Nav> getNavByPeFund(int pe_id) {

        List<Nav> returnNavList = new ArrayList<>();
        Dao navDao = new NavStatementDao();
        List<Domain.NavStatement> navList = ((NavStatementDao) navDao).getNavByPeFund(pe_id);

        for (Domain.NavStatement nav : navList) {
            returnNavList.add(new Nav(nav.getId(), nav.getNavDate(),
                    nav.getCapitalSize(), nav.getPensionFund().getName(), nav.getPeFund().getTicker()));
        }

        log.info(returnNavList);
        return returnNavList;
    }

    public Nav getNav(int id) {
        Dao navDao = new NavStatementDao();
        Domain.NavStatement nav = ((NavStatementDao) navDao).get(id);
        Nav resultNav = new Nav(nav.getId(), nav.getNavDate(),
                nav.getCapitalSize(), nav.getPensionFund().getName(), nav.getPeFund().getTicker());
        log.info(resultNav);
        return resultNav;
    }

    public void addNav(Nav nav) {
        Dao navDao = new NavStatementDao();
        Domain.NavStatement newNav = new Domain.NavStatement();
        newNav.setCapitalSize(nav.getCapitalSize());
        newNav.setNavDate(nav.getNavDate());

        Dao clienDao = new PensionFundDao();
        PensionFund client = ((PensionFundDao) clienDao).getFundByTicker(nav.getPensionFundName());
        newNav.setPensionFund(client);

        Dao fundDao = new PeFundDao();
        PeFund fund = ((PeFundDao) fundDao).getFundByName(nav.getPeFundName());
        newNav.setPeFund(fund);

        navDao.save(newNav);
    }

    public void deleteNav(int id) {
        Dao navDao = new NavStatementDao();
        navDao.delete(navDao.get(id));
    }

    public void updateNav(Nav nav, int id) {

        Dao navDao = new NavStatementDao();
        Domain.NavStatement oldNav = ((NavStatementDao) navDao).get(id);
        oldNav.setCapitalSize(nav.getCapitalSize());
        oldNav.setNavDate(nav.getNavDate());
        navDao.update(oldNav, null);
    }
}

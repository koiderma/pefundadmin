package Controller;

import BLL.DTO.CFlow;
import BLL.CashFlowService;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class CashFlowController {

    static Logger log = Logger.getLogger(CashFlowController.class.getName());

    @GetMapping("/cf/all/{id}")
    public List<CFlow> getAllCashFlowByPeFund(@PathVariable int id) {
        return new CashFlowService().getCashFlowByPeFund(id);
    }

    @GetMapping(value = "/cf/{id}")
    public ResponseEntity<Object> getCf(@PathVariable("id") int id) {

        try {
            return new ResponseEntity<>(new CashFlowService().getCashFlow(id), HttpStatus.OK);
        } catch (NullPointerException e) {
            log.error("Could not find cash-flow " + id);
            return new ResponseEntity<>("Could not find cash-flow", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/cf")
    public ResponseEntity<Object> addNav(@RequestBody CFlow cf) {

        LocalDate startDateEarliest = LocalDate.of(2010, 1, 1);

        if (cf.getCashFlow() == 0 || cf.getCashFlowType().isEmpty() || cf.getPeFundName().isEmpty() || cf.getPensionFundName().isEmpty()
                || cf.getPaymentDate().before(java.sql.Date.valueOf(startDateEarliest))) {
            log.error("Parameters not valid: " + cf);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else {
            log.info("New cashflow: " + cf);
            new CashFlowService().addCashFlow(cf);
            return new ResponseEntity<>(cf, HttpStatus.OK);
        }
    }

    @DeleteMapping("/cf/{id}")
    public ResponseEntity<Object> deleteCashFlow(@PathVariable int id) {

        log.info("Delete cashflow id: " + id);
        try {
            new CashFlowService().getCashFlow(id);
        } catch (NullPointerException e) {
            log.error("Could not find cash-flow " + id);
            return new ResponseEntity<>("Could not find cash-flow", HttpStatus.NOT_FOUND);
        }

        new CashFlowService().deleteCashFlow(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/cf/{id}")
    public ResponseEntity<Object> replaceCashFlow(@RequestBody CFlow cf, @PathVariable int id) {

        LocalDate startDateEarliest = LocalDate.of(2010, 1, 1);

        if (cf.getCashFlow() == 0 || id != cf.getId()
                || cf.getCashFlowType().isEmpty() || cf.getPeFundName().isEmpty() || cf.getPensionFundName().isEmpty()
                || cf.getPaymentDate().before(java.sql.Date.valueOf(startDateEarliest))) {
            log.error("Parameters not valid: " + cf);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else {
            log.info("Update cashflow id: " + id);
            new CashFlowService().updateCashFlow(cf, id);
            return new ResponseEntity<>(cf, HttpStatus.OK);
        }
    }

}

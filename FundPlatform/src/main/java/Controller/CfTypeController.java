package Controller;

import BLL.DTO.CfType;
import BLL.CfTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CfTypeController {

    @GetMapping("/cft")
    public List<CfType> getAllCashFlowTypes() {
        return new CfTypeService().getCashFlowTypes();
    }
}

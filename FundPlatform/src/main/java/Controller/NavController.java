package Controller;

import BLL.DTO.Nav;
import BLL.NavService;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class NavController {

    static Logger log = Logger.getLogger(NavController.class.getName());

    @GetMapping("/nav/all/{id}")
    public List<Nav> getAllNavByPeFund(@PathVariable int id) {
        return new NavService().getNavByPeFund(id);
    }

    @GetMapping(value = "/nav/{id}")
    public ResponseEntity<Object> getNav (@PathVariable("id") int id) {

        try {
            return new ResponseEntity<>(new NavService().getNav(id), HttpStatus.OK);
        } catch (NullPointerException e){
            log.error("Could not find nav " + id);
            return new ResponseEntity<>("Could not find nav", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/nav")
    public ResponseEntity<Object> addNav(@RequestBody Nav nav) {

        LocalDate startDateEarliest = LocalDate.of(2010, 1, 1);

        if ( nav.getCapitalSize() == 0 || nav.getPeFundName().isEmpty() || nav.getPensionFundName().isEmpty()
                || nav.getNavDate().before(java.sql.Date.valueOf(startDateEarliest))) {
            log.error("Parameters not valid: " + nav);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else {
            log.info("New nav: " + nav);
            new NavService().addNav(nav);
            return new ResponseEntity<>(nav, HttpStatus.OK);
        }
    }

    @DeleteMapping("/nav/{id}")
    public ResponseEntity<Object> deleteNav(@PathVariable int id) {
        log.info("Delete nav id: " + id);
        try {
            new NavService().getNav(id);
        } catch (NullPointerException e){
            log.error("Could not find nav " + id);
            return new ResponseEntity<>("Could not find nav", HttpStatus.NOT_FOUND);
        }

        new NavService().deleteNav(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/nav/{id}")
    public ResponseEntity<Object> replaceNav(@RequestBody Nav nav, @PathVariable int id) {

        LocalDate startDateEarliest = LocalDate.of(2010, 1, 1);

        if ( nav.getCapitalSize() == 0 || nav.getPeFundName().isEmpty() || nav.getPensionFundName().isEmpty()
                || id != nav.getId()
                || nav.getNavDate().before(java.sql.Date.valueOf(startDateEarliest))) {
            log.error("Parameters not valid: " + nav);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else {
            log.info("Update nav id: " + id);
            new NavService().updateNav(nav, id);
            return new ResponseEntity<>(nav, HttpStatus.OK);
        }
    }
}

package Controller;

import BLL.DTO.Commitment;
import BLL.CommitmentService;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommitmentController {

    static Logger log = Logger.getLogger(CommitmentController.class.getName());

    @GetMapping("/commitment/all/{id}")
    public List<Commitment> getAllCommitmentsByFund(@PathVariable int id) {
        return new CommitmentService().getCommitmentsByFund(id);
    }

    @GetMapping(value = "/commitment/{id}")
    public ResponseEntity<Object> getCommitment (@PathVariable("id") int id) {

        try {
            return new ResponseEntity<>(new CommitmentService().getCommitment(id), HttpStatus.OK);
        } catch (NullPointerException e){
            log.error("Could not find commitment " + id);
            return new ResponseEntity<>("Could not find commitment", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/commitment")
    public ResponseEntity<Object> addCommitment(@RequestBody Commitment com) {

        if (com.getCommitmentSize()==0 || com.getPeFundName().isEmpty()
                || com.getPensionFundName().isEmpty()){
            log.error("Parameters not valid: " + com);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else  {
            log.info("New cash flow: " + com);
            new CommitmentService().addCommitment(com);
            return new ResponseEntity<>(com, HttpStatus.OK);
        }
    }

    @DeleteMapping("/commitment/{id}")
    public ResponseEntity<Object> deleteCommitment(@PathVariable int id) {

        log.info("Delete commitment id: " + id);
        try {
            new CommitmentService().getCommitment(id);
        } catch (NullPointerException e){
            log.error("Could not find commitment " + id);
            return new ResponseEntity<>("Could not find commitment", HttpStatus.NOT_FOUND);
        }

        new CommitmentService().deleteCommitment(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/commitment/{id}")
    public ResponseEntity<Object> replaceCommitment(@RequestBody Commitment com, @PathVariable int id) {

        if (com.getCommitmentSize()==0 || com.getPeFundName().isEmpty() || id != com.getId()
                || com.getPensionFundName().isEmpty()){
            log.error("Parameters not valid: " + com);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else  {
            log.info("Update commitment id: " + id);
            new CommitmentService().updateCommitment(com, id);
            return new ResponseEntity<>(com, HttpStatus.OK);
        }
    }

}

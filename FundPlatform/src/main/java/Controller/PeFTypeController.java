package Controller;

import BLL.DTO.PefType;
import BLL.PefTypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PeFTypeController {
    @GetMapping("/peft")
    public List<PefType> getAllPeFundTypes() {
        return new PefTypeService().getPeFundTypes();
    }
}

package Controller;

import BLL.BackTestService;
import BLL.DTO.BackTestYearData;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BackTestController {

    @RequestMapping(value = "/backtest/{id}/{year}")
    public List<BackTestYearData> getClientCfModel(@PathVariable("id") int id, @PathVariable("year") int year ) {

        System.out.println(id+" "+ year);
        BackTestService bts = new BackTestService();

        return bts.getBackTestData(id, year);
    }
}

package Controller;

import BLL.DTO.Client;
import BLL.ClientService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ClientController {

    @RequestMapping("/clients")
    public List<Client> getAllClients() {
        return new ClientService().getAllClients();
    }
}

package Controller;

import BLL.DTO.PeFund;
import BLL.PeFundService;
import org.jboss.logging.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class PeFundController {

    static Logger log = Logger.getLogger(PeFundController.class.getName());

    @RequestMapping("/pefunds")
    public List<PeFund> getAllPeFunds() {
        return new PeFundService().getAllFunds();
    }

    @GetMapping(value = "/pefunds/{id}")
    public ResponseEntity<Object> getFund (@PathVariable("id") int id) {

        try {
            return new ResponseEntity<>(new PeFundService().getFund(id), HttpStatus.OK);
        } catch (NullPointerException e){
            log.error("Could not find pe-fund " + id);
            return new ResponseEntity<>("Could not find pe-fund", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/pefunds")
    public ResponseEntity<Object> addFund(@RequestBody PeFund newFund) {

        LocalDate startDateEarliest = LocalDate.of(2010, 1, 1);

        if (newFund.getTicker().isEmpty() || newFund.getName().isEmpty() || newFund.getPeFundType().isEmpty()
                || newFund.getStartDate().before(java.sql.Date.valueOf(startDateEarliest))
                || newFund.getInvestPeriodDate().before(newFund.getStartDate())
                || newFund.getEndDate().before(newFund.getStartDate())){
            log.error("Parameters not valid: " + newFund);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else  {
            log.info("New pe-fund: " + newFund);
            new PeFundService().addPeFund(newFund);
            return new ResponseEntity<>(newFund, HttpStatus.OK);
        }
    }

    @DeleteMapping("/pefunds/{id}")
    public ResponseEntity<Object> deleteFund(@PathVariable int id) {

        log.info("Delete pe-fund id: " + id);
        try {
            new PeFundService().getFund(id);
        } catch (NullPointerException e){
            log.error("Could not find pe-fund " + id);
            return new ResponseEntity<>("Could not find pe-fund", HttpStatus.NOT_FOUND);
        }

        new PeFundService().deletePeFund(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/pefunds/{id}")
    public ResponseEntity<Object> replaceFund(@RequestBody PeFund newFund, @PathVariable int id) {

        LocalDate startDateEarliest = LocalDate.of(2010, 1, 1);

        if (newFund.getTicker().isEmpty() || newFund.getName().isEmpty() || newFund.getPeFundType().isEmpty()
                || id != newFund.getId()
                || newFund.getStartDate().before(java.sql.Date.valueOf(startDateEarliest))
                || newFund.getInvestPeriodDate().before(newFund.getStartDate())
                || newFund.getEndDate().before(newFund.getStartDate())){
            log.error("Parameters not valid: " + newFund);
            return new ResponseEntity<>("Parameters not valid", HttpStatus.BAD_REQUEST);
        } else {
            log.info("Update pe-fund id: " + id + newFund);
            new PeFundService().updatePeFund(newFund, id);
            return new ResponseEntity<>(newFund, HttpStatus.OK);
        }
    }

}

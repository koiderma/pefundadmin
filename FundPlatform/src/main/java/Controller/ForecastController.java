package Controller;

import BLL.FundCashFlowService;
import BLL.FundCfModel;
import org.springframework.web.bind.annotation.*;


@RestController
public class ForecastController {

    @RequestMapping(value = "/forecast/{id}")
    public FundCfModel getClientCfModel(@PathVariable("id") int id) {
        FundCashFlowService fc = new FundCashFlowService();
        return fc.getFundCashFlowsByPensionFund(id);
    }

}


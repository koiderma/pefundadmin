package Domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PeFund")
public class PeFund implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pefund_id", updatable = false, nullable = false)
    @Getter
    @Setter
    private int id;

    @Column(name = "ticker")
    @Getter
    @Setter
    private String ticker;

    @Column(name = "name")
    @Getter
    @Setter
    private String name;

    @Column(name = "startdate")
    @Getter
    @Setter
    private java.sql.Date startDate;

    @Column(name = "investperioddate")
    @Getter
    @Setter
    private java.sql.Date investPeriodDate;

    @Column(name = "enddate")
    @Getter
    @Setter
    private java.sql.Date endDate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "peFund")
    private List<Commitment> commitments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "peFundNAV")
    private List<NavStatement> navStatements = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "peFundCF")
    private List<CashFlow> cashFlows = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "PEFTYPE_ID")
    private PeFType pefType;

    public PeFType getPefType() {
        return pefType;
    }

    public void setPefType(PeFType pefType) {
        this.pefType = pefType;
    }

    @Override
    public String toString() {
        return "Pe fund: " + this.getName() + " " + this.getTicker() + " " + this.getStartDate();
    }
}

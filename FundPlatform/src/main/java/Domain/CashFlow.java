package Domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CashFlow")
public class CashFlow implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cashflow_id", updatable = false, nullable = false)
    @Getter
    @Setter
    private int id;

    @Column(name = "paymentdate")
    @Getter
    @Setter
    private java.sql.Date paymentDate;

    @Column(name = "cashflow")
    @Getter
    @Setter
    private int cashFlow;

    @ManyToOne
    @JoinColumn(name = "PEFUND_ID")
    private PeFund peFundCF;

    @ManyToOne
    @JoinColumn(name = "PENSIONFUND_ID")
    private PensionFund pensionFundCF;

    @ManyToOne
    @JoinColumn(name = "CFTYPE_ID")
    private CfType cfType;

    public PeFund getPeFund() {
        return peFundCF;
    }

    public void setPeFund(PeFund peFund) {
        this.peFundCF = peFund;
    }

    public PensionFund getPensionFund() {
        return pensionFundCF;
    }

    public void setPensionFund(PensionFund pensionFund) {
        this.pensionFundCF = pensionFund;
    }

    public CfType getCfType() {
        return cfType;
    }

    public void setCfType(CfType cfType) {
        this.cfType = cfType;
    }

    @Override
    public String toString() {
        return "CashFlow: " + this.paymentDate + " " + getPeFund().getName() + " " + this.getCashFlow() + " " + getPensionFund().getName();
    }

}

package Domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PeFType")
public class PeFType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "peftype_id", updatable = false, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "pefType")
    private List<PeFund> peFunds = new ArrayList<>();
}

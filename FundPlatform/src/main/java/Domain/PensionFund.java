package Domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "PensionFund")
public class PensionFund implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pensionfund_id", updatable = false, nullable = false)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pensionFund")
    private List<Commitment> commitments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pensionFundNAV")
    private List<NavStatement> navStatements = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pensionFundCF")
    private List<CashFlow> cashFlows = new ArrayList<>();

}
package Domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Commitment")
public class Commitment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commitment_id", updatable = false, nullable = false)
    @Getter
    @Setter
    private int id;

    @Column(name = "commitmentsize")
    @Getter
    @Setter
    private int commitmentSize;

    @ManyToOne
    @JoinColumn(name = "PEFUND_ID")
    private PeFund peFund;

    @ManyToOne
    @JoinColumn(name = "PENSIONFUND_ID")
    private PensionFund pensionFund;

    public PeFund getPeFund() {
        return peFund;
    }

    public void setPeFund(PeFund peFund) {
        this.peFund = peFund;
    }

    public PensionFund getPensionFund() {
        return pensionFund;
    }

    public void setPensionFund(PensionFund pensionFund) {
        this.pensionFund = pensionFund;
    }

    @Override
    public String toString() {
        return "commitment: " + getPeFund().getName() + " " + this.getCommitmentSize() + " " + getPensionFund().getName();
    }

}

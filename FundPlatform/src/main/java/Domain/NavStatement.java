package Domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "NavStatement")
public class NavStatement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "navstatement_id", updatable = false, nullable = false)
    @Getter
    @Setter
    private int id;

    @Column(name = "navdate")
    @Getter
    @Setter
    private java.sql.Date navDate;

    @Column(name = "capitalsize")
    @Getter
    @Setter
    private int capitalSize;

    @ManyToOne
    @JoinColumn(name = "PEFUND_ID")
    private PeFund peFundNAV;

    @ManyToOne
    @JoinColumn(name = "PENSIONFUND_ID")
    private PensionFund pensionFundNAV;

    public PeFund getPeFund() {
        return peFundNAV;
    }

    public void setPeFund(PeFund peFund) {
        this.peFundNAV = peFund;
    }

    public PensionFund getPensionFund() {
        return pensionFundNAV;
    }

    public void setPensionFund(PensionFund pensionFund) {
        this.pensionFundNAV = pensionFund;
    }

    @Override
    public String toString() {
        return "NAV: " + getPeFund().getName() + " " + getCapitalSize() + " " + getPensionFund().getName() + " " + getNavDate();
    }


}

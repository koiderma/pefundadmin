import BLL.AppConstants;
import BLL.HistoricalDataService;
import DAL.CashFlowDao;
import DAL.NavStatementDao;
import Domain.Commitment;
import Domain.PeFund;
import Domain.PensionFund;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.junit.Test;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HistoricalDataServiceTest {

    @Test
    public void growthRateValidationResultsAreCorrect() {
        HistoricalDataService hds = new HistoricalDataService();
        assertEquals(0.12, hds.validateGrowthRate(0.0), 0.01);
        assertEquals(0.06, hds.validateGrowthRate(0.06), 0.01);
        assertEquals(0.15, hds.validateGrowthRate(0.15), 0.01);
        assertEquals(0.12, hds.validateGrowthRate(0.35), 0.01);
    }

    @Test
    public void growthRateYearlyCalculation() {

        List navList = new ArrayList();
        navList.add(170);
        navList.add(120);
        navList.add(100);
        List cfList = new ArrayList();
        cfList.add(-20);
        cfList.add(-5);
        cfList.add(-3);
        List<Double> growthList = new ArrayList<Double>();

        HistoricalDataService hds = new HistoricalDataService();
        hds.calculateGrowthRatesByYearList(navList, cfList, growthList);
        assertEquals(0.25, growthList.get(0), 0.01);
        assertEquals(0.15, growthList.get(1), 0.01);
        assertEquals(0.12, growthList.get(2), 0.01);

    }

    @Test
    public void checkNavAndCashFlowListCreationWorks() {

        NavStatementDao navDaoMock = mock(NavStatementDao.class);
        when(navDaoMock.getClientHistoricalNAV(anyInt(), any(Date.class))).thenReturn(100);
        CashFlowDao cfDaoMock = mock(CashFlowDao.class);
        when(cfDaoMock.getCashFlowsBetweenDates(anyInt(), any(Date.class), any(Date.class))).thenReturn(100);

        List navList = new ArrayList();
        List cfList = new ArrayList();

        HistoricalDataService hds = new HistoricalDataService();
        hds.setCfDao(cfDaoMock);
        hds.setNavDao(navDaoMock);
        hds.getFundCashFlowsAndNavLists(0, navList, cfList);

        assertEquals(10, navList.size());
        assertEquals(10, cfList.size());
        assertEquals(100, navList.get(0));
        assertEquals(100, cfList.get(0));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void navAndCashFlowListWithNoNAVCausesEmptyList() {

        NavStatementDao navDaoMock = mock(NavStatementDao.class);
        when(navDaoMock.getClientHistoricalNAV(anyInt(), any(Date.class))).thenReturn(-1);
        CashFlowDao cfDaoMock = mock(CashFlowDao.class);
        when(cfDaoMock.getCashFlowsBetweenDates(anyInt(), any(Date.class), any(Date.class))).thenReturn(100);

        List navList = new ArrayList();
        List cfList = new ArrayList();

        HistoricalDataService hds = new HistoricalDataService();
        hds.setCfDao(cfDaoMock);
        hds.setNavDao(navDaoMock);
        hds.getFundCashFlowsAndNavLists(0, navList, cfList);

        assertEquals(0, navList.size());
        assertEquals(0, cfList.size());
        assertEquals(null, navList.get(0));

    }

    @Test
    public void listExponentialSmoothingResult() {
        HistoricalDataService hds = new HistoricalDataService();
        List<Double> growthList = new ArrayList<Double>();
        growthList.add(0.15);
        growthList.add(0.12);
        growthList.add(0.10);
        growthList.add(0.05);

        double a = AppConstants.SMOOTHING_PARAMETER;
        double growthrate = (a * growthList.get(0) + a * (1 - a) * growthList.get(1) + a * Math.pow((1 - a), 2) * growthList.get(2) + a * Math.pow((1 - a), 3) * growthList.get(3))
                / (a + a * (1 - a) + a * Math.pow((1 - a), 2) + a * Math.pow((1 - a), 3));

        assertEquals(growthrate, hds.exponentialSmoothing(growthList), 0.01);

    }

    @Test
    public void growthPeriodEndDateAdjustment() {
        HistoricalDataService hds = new HistoricalDataService();

        hds.setCalculationDayDate(LocalDate.of(2020, 01, 15));
        assertEquals(LocalDate.of(2018, 12, 31), hds.getGrowthCalculationPeriodEndDate());

        hds.setCalculationDayDate(LocalDate.of(2020, 03, 30));
        assertEquals(LocalDate.of(2019, 12, 31), hds.getGrowthCalculationPeriodEndDate());

        hds.setCalculationDayDate(LocalDate.of(2020, 12, 31));
        assertEquals(LocalDate.of(2019, 12, 31), hds.getGrowthCalculationPeriodEndDate());

    }

    @Test
    public void FlowPeriodEndDateAdjustment() {
        HistoricalDataService hds = new HistoricalDataService();

        hds.setCalculationDayDate(LocalDate.of(2020, 01, 15));
        assertEquals(LocalDate.of(2020, 01, 15), hds.getFlowParameterCutOffDate());

        hds.setCalculationDayDate(LocalDate.of(2020, 03, 30));
        assertEquals(LocalDate.of(2020, 03, 30), hds.getFlowParameterCutOffDate());

        hds.setCalculationDayDate(LocalDate.of(2020, 12, 31));
        assertEquals(LocalDate.of(2020, 12, 31), hds.getFlowParameterCutOffDate());

    }

    @Test
    public void InFlowPeriodEndDateAdjustment() {
        HistoricalDataService hds = new HistoricalDataService();

        hds.setCalculationDayDate(LocalDate.of(2020, 01, 15));
        assertEquals(LocalDate.of(2019, 12, 31), hds.getInFlowParameterCutOffDate());

        hds.setCalculationDayDate(LocalDate.of(2020, 03, 30));
        assertEquals(LocalDate.of(2019, 12, 31), hds.getInFlowParameterCutOffDate());

        hds.setCalculationDayDate(LocalDate.of(2020, 12, 31));
        assertEquals(LocalDate.of(2020, 12, 31), hds.getInFlowParameterCutOffDate());

    }

    @Test
    public void firstYearParameterValidationResultsAreCorrect() {
        HistoricalDataService hds = new HistoricalDataService();
        assertEquals(0.25, hds.validateFirstYearParameter(0.0), 0.01);
        assertEquals(0.15, hds.validateFirstYearParameter(0.15), 0.01);
        assertEquals(0.22, hds.validateFirstYearParameter(0.22), 0.01);
        assertEquals(0.25, hds.validateFirstYearParameter(0.40), 0.01);
    }

    @Test
    public void firstYearParameterMapFunctionsProperlyOnNewData() {

        Map<Integer, List<Double>> firstYear = new HashMap();
        firstYear.put(2014, new ArrayList());
        firstYear.get(2014).add(0.20);
        LocalDate cutOffDate = LocalDate.now();
        LocalDate startDate = LocalDate.of(2012, 12, 30);
        PeFund pef = new PeFund();
        pef.setStartDate(java.sql.Date.valueOf(startDate));
        Commitment com = new Commitment();
        com.setCommitmentSize(5000000);
        com.setPeFund(pef);

        HistoricalDataService hds = new HistoricalDataService() {

            @Override
            public int getTotalFlow(int client_id, Commitment com, LocalDate startDate, LocalDate endDate) {
                return -500000;
            }
        };

        hds.getFirstYearOutFlowAllData(0, firstYear, cutOffDate, com);

        assertEquals(0.1, firstYear.get(2013).get(0), 0.01);

        com.getPeFund().setStartDate(java.sql.Date.valueOf(startDate.plusYears(1)));
        hds.getFirstYearOutFlowAllData(0, firstYear, cutOffDate, com);

        assertEquals(0.1, firstYear.get(2014).get(1), 0.01);

    }

    @Test
    public void mapAveragingPerKeyCalculations() {

        Map<Integer, List<Double>> firstYear = new HashMap();
        firstYear.put(2014, new ArrayList());
        firstYear.get(2014).add(0.20);
        firstYear.get(2014).add(0.40);
        firstYear.put(2015, new ArrayList());
        firstYear.get(2015).add(0.10);
        firstYear.get(2015).add(0.30);
        firstYear.get(2015).add(0.80);

        HistoricalDataService hds = new HistoricalDataService();
        Map<Integer, Double> returnMap = new HashMap();
        returnMap = hds.calculateMapAverages(firstYear);

        assertEquals(0.3, returnMap.get(2014), 0.01);
        assertEquals(0.4, returnMap.get(2015), 0.01);

    }

    @Test
    public void mapExponentialSmoothingResult() {

        HistoricalDataService hds = new HistoricalDataService();

        Map<Integer, Double> dataMap = new HashMap();
        dataMap.put(2011, 0.12);
        dataMap.put(2013, 0.15);
        dataMap.put(2017, 0.22);
        dataMap.put(2019, 0.28);

        double a = AppConstants.SMOOTHING_PARAMETER;
        double parameter = (a * dataMap.get(2019) + a * (1 - a) * dataMap.get(2017) + a * Math.pow((1 - a), 2) * dataMap.get(2013) + a * Math.pow((1 - a), 3) * dataMap.get(2011))
                / (a + a * (1 - a) + a * Math.pow((1 - a), 2) + a * Math.pow((1 - a), 3));

        assertEquals(parameter, hds.getExponentialSmoothedAverage(dataMap), 0.01);

    }

    @Test
    public void secondYearParameterMapFunctionsProperlyOnNewData() {

        Map<Integer, List<Double>> secondYear = new HashMap();
        secondYear.put(2014, new ArrayList());
        secondYear.get(2014).add(0.20);
        LocalDate cutOffDate = LocalDate.now();
        LocalDate startDate = LocalDate.of(2012, 12, 30);
        PeFund pef = new PeFund();
        pef.setStartDate(java.sql.Date.valueOf(startDate));
        Commitment com = new Commitment();
        com.setCommitmentSize(5000000);
        com.setPeFund(pef);

        HistoricalDataService hds = new HistoricalDataService() {

            @Override
            public int getTotalFlow(int client_id, Commitment com, LocalDate startDate, LocalDate endDate) {
                return -500000;
            }
        };

        double secYear = (double) 500000 / (5000000 - 1 * 500000);
        hds.getSecondYearOutFlowAllData(0, secondYear, cutOffDate, com);
        assertEquals(secYear, secondYear.get(2014).get(1), 0.01);

        com.getPeFund().setStartDate(java.sql.Date.valueOf(startDate.plusYears(1)));
        hds.getSecondYearOutFlowAllData(0, secondYear, cutOffDate, com);
        assertEquals(secYear, secondYear.get(2015).get(0), 0.01);

    }

    @Test
    public void secondYearParameterValidationResultsAreCorrect() {
        HistoricalDataService hds = new HistoricalDataService();
        assertEquals(0.33, hds.validateSecondYearOutFlowParameter(0.0), 0.01);
        assertEquals(0.15, hds.validateSecondYearOutFlowParameter(0.15), 0.01);
        assertEquals(0.22, hds.validateSecondYearOutFlowParameter(0.22), 0.01);
        assertEquals(0.33, hds.validateSecondYearOutFlowParameter(0.50), 0.01);
    }

    @Test
    public void otherYearParameterValidationResultsAreCorrect() {
        HistoricalDataService hds = new HistoricalDataService();
        assertEquals(0.50, hds.validateOtherYearOutFlowParameter(0.0), 0.01);
        assertEquals(0.20, hds.validateOtherYearOutFlowParameter(0.20), 0.01);
        assertEquals(0.35, hds.validateOtherYearOutFlowParameter(0.35), 0.01);
        assertEquals(0.50, hds.validateOtherYearOutFlowParameter(0.65), 0.01);
    }

    @Test
    public void otherYearParameterMapFunctionsProperlyOnNewData() {

        Map<Integer, List<Double>> otherYear = new HashMap();
        otherYear.put(2014, new ArrayList());
        otherYear.get(2014).add(0.20);
        LocalDate cutOffDate = LocalDate.now();
        LocalDate startDate = LocalDate.of(2011, 12, 30);
        PeFund pef = new PeFund();
        pef.setStartDate(java.sql.Date.valueOf(startDate));
        Commitment com = new Commitment();
        com.setCommitmentSize(5000000);
        com.setPeFund(pef);

        HistoricalDataService hds = new HistoricalDataService() {

            @Override
            public int getTotalFlow(int client_id, Commitment com, LocalDate startDate, LocalDate endDate) {
                return -500000;
            }
        };

        double thirdYear = (double) 500000 / (5000000 - 2 * 500000);
        double fourthYear = (double) 500000 / (5000000 - 3 * 500000);
        double fifthYear = (double) 500000 / (5000000 - 4 * 500000);

        hds.getOtherYearOutFlowAllData(0, otherYear, cutOffDate, com);
        assertEquals(thirdYear, otherYear.get(2014).get(1), 0.01);
        assertEquals(fourthYear, otherYear.get(2015).get(0), 0.01);
        assertEquals(fifthYear, otherYear.get(2016).get(0), 0.01);

    }

    @Test
    public void distributionParameterRegressionAnalysis() {

        NavStatementDao navDaoMock = mock(NavStatementDao.class);
        when(navDaoMock.getNavByFundsAndDate(anyInt(), anyInt(), any(Date.class))).thenReturn(1000);
        CashFlowDao cfDaoMock = mock(CashFlowDao.class);
        when(cfDaoMock.getCashInFlowsBetweenDates(anyInt(), anyInt(), any(Date.class), any(Date.class))).thenReturn(100);

        LocalDate startDate = LocalDate.of(2012, 12, 30);
        PeFund pef = new PeFund();
        pef.setStartDate(java.sql.Date.valueOf(startDate));
        pef.setEndDate(java.sql.Date.valueOf(startDate.plusYears(10)));
        pef.setId(0);
        PensionFund pensionf = new PensionFund();
        pensionf.setId(0);
        Commitment com = new Commitment();
        com.setCommitmentSize(5000000);
        com.setPeFund(pef);
        com.setPensionFund(pensionf);

        Double growthRate = 0.12;
        SimpleRegression regression = new SimpleRegression();
        HistoricalDataService hds = new HistoricalDataService();
        hds.setCfDao(cfDaoMock);
        hds.setNavDao(navDaoMock);

        hds.getDistributionParameterRegressionData(0, growthRate, regression, com);
        assertEquals(0, regression.getSlope(), 0.01);
        assertEquals(-2.42, regression.getIntercept(), 0.01);
    }
}

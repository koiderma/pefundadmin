import BLL.FundCashFlowService;
import BLL.FundCfModel;
import DAL.CommitmentDao;
import Domain.*;
import org.junit.Test;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FundCashFlowServiceTest {

    @Test
    public void oneFundCashOutFlowForecastCalculationNewFund() {

        FundCashFlowService fcfs = new FundCashFlowService();
        double rcp1 = 0.15;
        double rcp2 = 0.25;
        double rcp3 = 0.35;
        fcfs.setRcp1(rcp1);
        fcfs.setRcp2(rcp2);
        fcfs.setRcpNext(rcp3);

        FundCfModel cfModel = new FundCfModel();
        long daysFundWorking = 0;
        long fundMaturity = 3650;
        int commitmentSize = 10000000;
        int paidInCapital = 0;

        fcfs.calculatePeFundOutFlowForecast(cfModel, daysFundWorking, fundMaturity, commitmentSize, paidInCapital);

        int cashOutFirstYear = (int) Math.round(-rcp1 * commitmentSize);
        int cashOutSecondYear = (int) Math.round(-rcp2 * Math.round(commitmentSize + cashOutFirstYear));
        int cashOutThirdYear = (int) Math.round(-rcp3 * Math.round(commitmentSize + cashOutFirstYear + cashOutSecondYear));
        assertEquals(cashOutFirstYear, (int) cfModel.getCashOutFlows().get(0));
        assertEquals(cashOutSecondYear, (int) cfModel.getCashOutFlows().get(1));
        assertEquals(cashOutThirdYear, (int) cfModel.getCashOutFlows().get(2));

    }

    @Test
    public void oneFundCashOutFlowForecastCalculationMidTermFund() {

        FundCashFlowService fcfs = new FundCashFlowService();
        double rcp3 = 0.30;
        fcfs.setRcpNext(rcp3);

        FundCfModel cfModel = new FundCfModel();
        long daysFundWorking = 1200;
        long fundMaturity = 3650;
        int commitmentSize = 1000000;
        int paidInCapital = 500000;

        fcfs.calculatePeFundOutFlowForecast(cfModel, daysFundWorking, fundMaturity, commitmentSize, paidInCapital);

        int cashOutFirstYear = (int) Math.round(-rcp3 * (commitmentSize - paidInCapital));
        int cashOutSecondYear = (int) Math.round(-rcp3 * (commitmentSize - paidInCapital + cashOutFirstYear));
        int cashOutThirdYear = (int) Math.round(-rcp3 * (commitmentSize - paidInCapital + cashOutFirstYear + cashOutSecondYear));
        assertEquals(cashOutFirstYear, (int) cfModel.getCashOutFlows().get(0));
        assertEquals(cashOutSecondYear, (int) cfModel.getCashOutFlows().get(1));
        assertEquals(cashOutThirdYear, (int) cfModel.getCashOutFlows().get(2));
        System.out.println(cfModel);
    }

    @Test
    public void calculateFundPayInCashFlow() {

        List<CashFlow> flows = generateCashFlowList();
        int expectedAnswer = -(flows.get(0).getCashFlow() + flows.get(1).getCashFlow() + flows.get(2).getCashFlow());
        FundCashFlowService fcfs = new FundCashFlowService();
        assertEquals(expectedAnswer, fcfs.getPayInCashFlow(flows));

    }

    @Test
    public void fundMaturityCalculationMidTermFund() {

        PeFund pef = new PeFund();
        LocalDate startDate = LocalDate.of(2012, 12, 30);
        pef.setStartDate(java.sql.Date.valueOf(startDate));
        LocalDate endDate = LocalDate.of(2022, 12, 30);
        pef.setEndDate(java.sql.Date.valueOf(endDate));
        Commitment com = new Commitment();
        com.setCommitmentSize(5000000);
        com.setPeFund(pef);
        double navValue = 3000000;
        long daysFundWorking = 1000;
        FundCashFlowService fcfs = new FundCashFlowService();

        long expectedAnswer = ChronoUnit.DAYS.between(startDate, endDate);
        assertEquals(expectedAnswer, fcfs.getFundMaturity(pef, com, navValue, daysFundWorking));

    }

    @Test
    public void fundMaturityCalculationWithFundExtension() {

        PeFund pef = new PeFund();
        LocalDate startDate = LocalDate.of(2012, 12, 30);
        pef.setStartDate(java.sql.Date.valueOf(startDate));
        LocalDate endDate = LocalDate.of(2020, 12, 30);
        pef.setEndDate(java.sql.Date.valueOf(endDate));

        Commitment com = new Commitment();
        com.setCommitmentSize(5000000);
        com.setPeFund(pef);
        double navValue = 3000000;
        long daysFundWorking = 2630;
        FundCashFlowService fcfs = new FundCashFlowService();

        long expectedAnswer = ChronoUnit.DAYS.between(startDate, endDate.plusYears(1));
        assertEquals(expectedAnswer, fcfs.getFundMaturity(pef, com, navValue, daysFundWorking));

    }

    @Test
    public void fundCashFlowAfterNavDateCalculation() {

        List<CashFlow> flows = generateCashFlowList();
        LocalDate navDate = LocalDate.of(2015, 12, 30);
        Date navD = java.sql.Date.valueOf(navDate);

        FundCashFlowService fcfs = new FundCashFlowService();

        assertEquals(-100, fcfs.calculateCashFlowAfterNAV(flows, navD));

    }

    @Test
    public void oneFundNavAndInFlowCalculation() {

        NavStatement nav = new NavStatement();
        nav.setCapitalSize(2000000);
        Commitment com = new Commitment();
        com.setCommitmentSize(3000000);
        long daysFundWorking = 1500;
        long fundMaturity = 3650;
        List<CashFlow> flows = new ArrayList<>();

        FundCfModel cfModel = new FundCfModel();
        List<Integer> cashOutFlows = new ArrayList();
        cashOutFlows.add(-100000);
        cashOutFlows.add(-85000);
        cashOutFlows.add(-60000);
        cashOutFlows.add(-5000);
        cashOutFlows.add(-4000);
        cashOutFlows.add(-250);
        cfModel.setCashOutFlows(cashOutFlows);

        FundCashFlowService fcfs = new FundCashFlowService() {

            @Override
            public int calculateCashFlowAfterNAV(List<CashFlow> flows, java.util.Date navDate) {
                return 0;
            }
        };
        fcfs.setBow(1.26);
        fcfs.setGrowthRate(0.12);

        cfModel = fcfs.getPeFundNAVInflowByPensionFund(cfModel, daysFundWorking, fundMaturity, nav, flows, com);

        double rdFirstYear = Math.pow(((double) daysFundWorking / (double) fundMaturity), 1.26);
        double firstYearInFlow = (rdFirstYear * (nav.getCapitalSize() * 1.12));
        double navFirstYear = (nav.getCapitalSize() * 1.12 - firstYearInFlow - cashOutFlows.get(0));

        double rdSecondYear = Math.pow(((double) (daysFundWorking + 365) / (double) fundMaturity), 1.26);
        double secondYearInFlow = (rdSecondYear * (navFirstYear * 1.12));
        double navSecondYear = (navFirstYear * 1.12 - secondYearInFlow - cashOutFlows.get(1));

        assertEquals((int) firstYearInFlow, (int) cfModel.getCashInFlows().get(0));
        assertEquals((int) navFirstYear, (int) cfModel.getNavs().get(0));
        assertEquals((int) secondYearInFlow, (int) cfModel.getCashInFlows().get(1));
        assertEquals((int) navSecondYear, (int) cfModel.getNavs().get(1));

        int navListSize = cfModel.getNavs().size();
        assertEquals(0, (int) cfModel.getNavs().get(navListSize - 1));

    }

    @Test
    public void aggregatePortfolioFundCashFlowModels() {

        List<Commitment> comList = generateCommitmentList();
        CommitmentDao commitmentDaoMock = mock(CommitmentDao.class);

        when(commitmentDaoMock.getCommitmentByPensionFundsBeforeDate(anyInt(), any(Date.class))).thenReturn(comList);

        FundCfModel newModel = generateOneFundModel();
        FundCfModel portfolio = generatePortfolioModel();

        FundCashFlowService fcfs = new FundCashFlowService() {
            @Override
            public FundCfModel getPeFundCashFlowsByPensionFund(int pf_id, PeFund peFund, Commitment com) {
                return newModel;
            }
        };

        fcfs.setAllAggregate(portfolio);
        fcfs.setCommitmentDao(commitmentDaoMock);
        fcfs.setCutOffDate(java.sql.Date.valueOf(LocalDate.now()));
        fcfs.setAllAggregate(portfolio);

        fcfs.calculateClientCashFlowEstimates(0);

        FundCfModel answerPortfolio = generatePortfolioModel();
        int latestNavAnswer = answerPortfolio.getLatestNAV() + 2 * newModel.getLatestNAV();
        int firstYearOutflow = answerPortfolio.getCashOutFlows().get(0) + 2 * newModel.getCashOutFlows().get(0);
        int firstYearInflow = answerPortfolio.getCashInFlows().get(0) + 2 * newModel.getCashInFlows().get(0);
        int firstYearNav = answerPortfolio.getNavs().get(0) + 2 * newModel.getNavs().get(0);

        assertEquals(latestNavAnswer, fcfs.getAllAggregate().getLatestNAV());
        assertEquals(firstYearOutflow, (int) fcfs.getAllAggregate().getCashOutFlows().get(0));
        assertEquals(firstYearInflow, (int) fcfs.getAllAggregate().getCashInFlows().get(0));
        assertEquals(firstYearNav, (int) fcfs.getAllAggregate().getNavs().get(0));

    }

    private FundCfModel generatePortfolioModel() {
        FundCfModel portfolio = new FundCfModel();
        portfolio.setLatestNAV(5000);
        List<Integer> outFlows = new ArrayList<>();
        outFlows.add(-1000);
        outFlows.add(-900);
        outFlows.add(-600);
        outFlows.add(-300);
        portfolio.setCashOutFlows(outFlows);
        List<Integer> inFlows = new ArrayList<>();
        inFlows.add(3000);
        inFlows.add(2500);
        inFlows.add(1000);
        inFlows.add(900);
        portfolio.setCashInFlows(inFlows);
        List<Integer> navs = new ArrayList<>();
        navs.add(3300);
        navs.add(1400);
        navs.add(800);
        navs.add(0);
        portfolio.setNavs(navs);
        return portfolio;
    }

    private FundCfModel generateOneFundModel() {
        FundCfModel newModel = new FundCfModel();
        newModel.setLatestNAV(1000);
        List<Integer> outFlows = new ArrayList<>();
        outFlows.add(-80);
        outFlows.add(-50);
        outFlows.add(-20);
        newModel.setCashOutFlows(outFlows);
        List<Integer> inFlows = new ArrayList<>();
        inFlows.add(650);
        inFlows.add(330);
        inFlows.add(200);
        newModel.setCashInFlows(inFlows);
        List<Integer> navs = new ArrayList<>();
        navs.add(650);
        navs.add(330);
        navs.add(200);
        newModel.setNavs(navs);
        return newModel;
    }

    private List<Commitment> generateCommitmentList() {
        List<Commitment> comList = new ArrayList();
        Commitment com1 = new Commitment();
        PeFund pef1 = new PeFund();
        com1.setPeFund(pef1);
        Commitment com2 = new Commitment();
        com2.setPeFund(pef1);
        comList.add(com1);
        comList.add(com2);
        return comList;
    }

    private List<CashFlow> generateCashFlowList() {
        CfType contribution = new CfType();
        contribution.setId(0);
        CfType equalization = new CfType();
        equalization.setId(3);

        CashFlow cf1 = new CashFlow();
        cf1.setCashFlow(-200);
        cf1.setCfType(contribution);
        LocalDate pDate = LocalDate.of(2012, 12, 30);
        cf1.setPaymentDate(Date.valueOf(pDate));

        CashFlow cf2 = new CashFlow();
        cf2.setCashFlow(-350);
        cf2.setCfType(contribution);
        LocalDate pDate2 = LocalDate.of(2014, 12, 30);
        cf2.setPaymentDate(Date.valueOf(pDate2));

        CashFlow cf3 = new CashFlow();
        cf3.setCashFlow(100);
        cf3.setCfType(equalization);
        LocalDate pDate3 = LocalDate.of(2019, 12, 30);
        cf3.setPaymentDate(Date.valueOf(pDate3));

        List<CashFlow> flows = new ArrayList<>();
        flows.add(cf1);
        flows.add(cf2);
        flows.add(cf3);
        return flows;
    }

}
